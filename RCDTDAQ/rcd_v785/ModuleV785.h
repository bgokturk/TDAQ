/********************************************************/
/*							*/
/* Date:   1 November 2006				*/ 
/* Author: Markus Joos					*/
/*							*/
/*** C 2006 - The software with that certain something **/

#ifndef MODULEV785_H
#define MODULEV785_H

#include "ROSCore/ReadoutModule.h"

namespace ROS 
{
  class ModuleV785 : public ReadoutModule
  {
  public:
    virtual void setup(DFCountedPointer<Config> configuration);
    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void prepareForRun(const daq::rc::TransitionCmd&);
    virtual void stopDC(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&);
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void clearInfo();
    virtual DFCountedPointer < Config > getInfo();
    ModuleV785();
    virtual ~ModuleV785()  noexcept;

    virtual const std::vector<DataChannel *> *channels();
    
  private:
    DFCountedPointer<Config> m_configuration;
    std::vector<DataChannel *> m_dataChannels;

    // channel parameters
    u_int m_id;
    u_int m_rolPhysicalAddress;

    // VMEbus parameters of a module
    u_int m_vmeAddress;
    u_int m_vmeWindowSize;
    u_int m_vmeAddressModifier;
    unsigned long m_vmeVirtualPtr;
    u_int m_iped;
    int m_vmeScHandle;
    v785_regs_t *m_v785;
  };

  inline const std::vector<DataChannel *> *ModuleV785::channels()
  {
    return &m_dataChannels;
  }  
}
#endif // MODULEV785_H
