/*
 * adapted to BL4S Jan,18 J.Petersen
 */

#ifndef mmDisplay_h
#define mmDisplay_h

#include <list>
#include <string>
#include <vector>

class CConfiguration;
class CReceiver;
class CReceiverFile;
class CEventDecoder;
class CMMEvent;
class CUDPData;
class CEventVectors;
class CPublisher;

   CConfiguration*	m_config;
   CReceiver*           m_receiver;
   CReceiverFile*       m_receiverFile;
   CEventDecoder*       m_decoder;
   CPublisher*          m_publisher;
   CEventVectors*       m_eventVectors;

   std::list <CUDPData*>        m_datacontainer;
   std::list <CMMEvent*>        m_eventcontainer;

   CEvent::event_type_type      m_preset_event_type;
   CConfiguration::runtype_t    m_preset_run_type;

// must process m_jump + m_events events, first skip, then display
   int m_events; // number of events to display
   int m_jump; // # events to skip = first event number to display 
   int m_projection; // plot track projection: the measured points

   int m_zsEnable;

   std::map<int,int> m_stripToChannelChamber;
   std::vector< std::map<int,int> > m_stripToChannel;  // ALL chanbers

   int m_lowStrip, m_highStrip;        // for ONE chamber
   std::pair<unsigned,unsigned> m_sRange;

   std::vector<std::string> m_chamberNames;   // build our own chamber names
   std::map <std::string,float> m_zPos;
   std::map <std::string,float> m_angle;
   std::map <std::string,float> m_pitch;
   std::map <std::string,float> m_maxStrips;
   float m_deltaZpos;   // largest distance between chambers

   BL4SConfig* m_cfg;
   BL4SGeometry* m_geo;

   std::map <std::string,float> m_xPos;   // centre of chamber
   std::map <std::string,float> m_yPos;
   std::map <std::string,float> m_xLength;  // length of chamber
   std::map <std::string,float> m_yLength;
   std::map <std::string,float> m_zLength;

// along z
   std::string m_firstChamber;
   std::string m_lastChamber;

   std::map<std::string,microMegasMon*> m_mmDetector;
 
// MAPS ..  
   std::map <std::string,float> m_xyRel;   // relative position of HIT (Qmax) 
   std::map <std::string,float> m_xRel;   //  X(Qmax) 
   std::map <std::string,float> m_yRel;   //  Y(Qmax) 

// the space coordinates
   std::map <std::string,std::vector<float>> m_xyz;

   std::vector<ScintillatorMon*> m_scintDetector;

   time_t m_run_start_time;
   bool   m_save_data_flag;

#endif
