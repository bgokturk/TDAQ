#ifndef __MONITOR_QMAX__ 
#define __MONITOR_QMAX__ 

#include "MonitorBase.h"

#include <vector>
#include <map>
#include <time.h>

#include <TH1.h>
#include <TGraph.h>
#include <TFile.h>

class CUDPData;
class CEventDecoder;
class CEventVectors;

 class MonitorQmaxRealTime: public MonitorBase {
// class MonitorQmaxRealTime {
  public:
    MonitorQmaxRealTime();
    MonitorQmaxRealTime(std::string name, CConfiguration* config, std::string chamberName);
    virtual ~MonitorQmaxRealTime();

    void SetQmaxMeanTime(int qt) {m_qmaxMeanTime =  qt; }
    void SetDataContainerPtr(std::list <CUDPData*>* dc) {m_datacontainerPtr =  dc; }
    void SetEventContainerPtr(std::list <CMMEvent*>* ec) {m_eventcontainerPtr =  ec; }

    void SetHistProvider(OHRootProvider* prov) { m_histProvider = prov; }

    void SetEventDecoder(CEventDecoder* evd) {m_decoder = evd; }
    void SetEventVectors(CEventVectors* evv) {m_eventVectors =  evv; }

    int buildEventVectors(std::vector<unsigned int>& rawEvent);

    virtual bool Process(std::vector<unsigned int>&  ev);
    virtual void Print(std::vector<unsigned int>& ev);
    virtual void WriteToFile();
    virtual void PrepareRootFile(std::string runNumberStr);
    virtual void PublishHists();

    bool IsTimeToRecord();

  private:
    TH1F * m_maxQ_event;
    TGraph* m_maxQ_time;
    int m_point;

    int m_qmaxMeanTime;

    OHRootProvider* m_histProvider;

    bool m_firstEvent;

    std::string m_chamberName;
    TFile * m_outputFile;
    std::string m_MonitorName;

    std::list <CUDPData*>*        m_datacontainerPtr;
    std::list <CMMEvent*>*        m_eventcontainerPtr;

    CConfiguration*      m_config;
    CEventDecoder*       m_decoder;
    CEventVectors*       m_eventVectors;

    time_t m_lastRecordTime;
};

  
#endif
