#ifndef __MONITOR_CHAMBER__ 
#define __MONITOR_CHAMBER__ 

#include "MonitorBase.h"

#include <vector>
#include <list>
#include <map>
#include <utility>

#include <TH1.h>
#include <TH2.h>
#include <TFile.h>

class CUDPData;
class CEventDecoder;
class CEventVectors;

 class MonitorChamber: public MonitorBase {
// class MonitorChamber {
  public:
    MonitorChamber();
    MonitorChamber(std::string name, CConfiguration* config, std::string chamberName, int stripNumber,
                   std::string runNumber, bool publish);
    virtual ~MonitorChamber();

    void SetDataContainerPtr(std::list <CUDPData*>* dc) {m_datacontainerPtr =  dc; }
    void SetEventContainerPtr(std::list <CMMEvent*>* ec) {m_eventcontainerPtr =  ec; }

    void SetHistProvider(OHRootProvider* prov) { m_histProvider = prov; }

    void SetEventDecoder(CEventDecoder* evd) {m_decoder = evd; }
    void SetEventVectors(CEventVectors* evv) {m_eventVectors =  evv; }
    void SetTimeRange(int tl, int th){m_timeLow = tl; m_timeHigh = th;}

    int m_zsEnable;

    int buildEventVectors(std::vector<unsigned int>& rawEvent);
    void pedestalVectors();

    virtual bool Process(std::vector<unsigned int>&  ev);
    virtual void Print(std::vector<unsigned int>& ev);
    virtual void WriteToFile();
    virtual void PrepareRootFile(std::string runNumberStr);
    virtual void PublishHists();

  private:
    std::vector<TH1F*> m_maxQ;
    std::vector<TH1I*> m_maxQ_event;
    std::vector<TH1I*> m_maxQ_event_cluster;
    std::vector<TH1I*> m_maxQ_event_strip;
    std::vector<TH1I*> m_maxQ_event_channel;
    std::vector<TH1I*> m_maxQ_event_time;
    std::vector<TH1I*> m_multiplicity;
    std::vector<TH1I*> m_noChannels;
    std::vector<TH2I*> m_correlation;

    TH1I* m_number_emptyEvents;

    std::vector<TH1F*> m_Q_strip;

    OHRootProvider* m_histProvider;

    bool m_firstEvent;

    int m_lowStrip, m_highStrip;	// for ONE chamber
    int m_timeLow, m_timeHigh;
    std::map<int,int> m_stripToChannelChamber;
    std::vector< std::map<int,int> > m_stripToChannel;  // ALL chanbers
    std::map<std::string,int> m_nameMap;
    std::pair<unsigned,unsigned> m_sRange;

// pedestal VECTORS
    std::vector<float> m_pedestal_mean;
    std::vector<float> m_pedestal_stdev;

/******** USER DEFINITION *****************/
    float m_sigma_cut_factor = 3.0;
/******************************************/

    TFile * m_outputFile;
    std::string m_MonitorName;

    std::list <CUDPData*>*        m_datacontainerPtr;
    std::list <CMMEvent*>*        m_eventcontainerPtr;

    CConfiguration*      m_config;
    CEventDecoder*       m_decoder;
    CEventVectors*       m_eventVectors;
    
    bool  m_computeCorrelations;
};

  
#endif
