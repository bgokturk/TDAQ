#ifndef __MONITOR_BEAM__ 
#define __MONITOR_BEAM__ 

#include "MonitorBase.h"

#include <vector>
#include <list>
#include <map>

#include <TH1.h>
#include <TH2.h>
#include <TFile.h>

class CUDPData;
class CEventDecoder;
class CEventVectors;

 class MonitorBeam: public MonitorBase {
// class MonitorBeam {
  public:
    MonitorBeam();
    MonitorBeam(std::string name, CConfiguration* config, std::string runNumber, bool publish);
    virtual ~MonitorBeam();

    void SetDataContainerPtr(std::list <CUDPData*>* dc) {m_datacontainerPtr =  dc; }
    void SetEventContainerPtr(std::list <CMMEvent*>* ec) {m_eventcontainerPtr =  ec; }

    void SetHistProvider(OHRootProvider* prov) { m_histProvider = prov; }

    void SetEventDecoder(CEventDecoder* evd) {m_decoder = evd; }
    void SetEventVectors(CEventVectors* evv) {m_eventVectors =  evv; }

    int buildEventVectors(std::vector<unsigned int>& rawEvent);
    void pedestalVectors();
    void getGeometry(void);

    virtual bool Process(std::vector<unsigned int>&  ev);
    virtual void Print(std::vector<unsigned int>& ev);
    virtual void WriteToFile();
    virtual void PrepareRootFile(std::string runNumberStr);
    virtual void PublishHists();

  private:

    OHRootProvider* m_histProvider;

    bool m_firstEvent;

// pedestal VECTORS
    std::vector<float> m_pedestal_mean;
    std::vector<float> m_pedestal_stdev;

/******** USER DEFINITION *****************/
    float m_sigma_cut_factor = 3.0;
/******************************************/

    TFile * m_outputFile;
    std::string m_MonitorName;

    std::list <CUDPData*>*        m_datacontainerPtr;
    std::list <CMMEvent*>*        m_eventcontainerPtr;

    CConfiguration*      m_config;
    CEventDecoder*       m_decoder;
    CEventVectors*       m_eventVectors;

   std::vector<std::string> m_chamberNames;
   std::map <std::string,float> m_zPos;
   std::map <std::string,float> m_pitch;
   std::map <std::string,float> m_maxStrips;

   std::vector<TH1F*> m_xRel;
   std::vector<TH2F*> m_correlation;

};

  
#endif
