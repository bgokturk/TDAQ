/**************************************************/
/*  Display  of MicroMegas RD51		          */
/*  runs as a TApplication			  */
/*  and uses Eve                                  */
/*                                                */
/*  author:  J.Petersen 		          */
/*  2018/01					  */
/*  adapt to zero suppression Jan. 2018           */
/**************************************************/

#include "TEveManager.h"
#include "TEveEventManager.h"
#include "TEveViewer.h"
#include <TApplication.h>

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <boost/format.hpp>

#include <time.h>

#include "BL4SConfig.h"
#include "BL4SGeometry.h"
#include "BL4SMonitorDetectors.h"

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include "DAQEventReader.h"
#include "FileEventReader.h"

// MM classes
#include "CConfiguration.h"
#include "CEventDecoder.h"
#include "CEventVectors.h"
#include "CUDPData.h"
#include "CEvent.h"

#include <TEnv.h>
#include <TString.h>
#include <TApplication.h>

#include "mmDisplay.h"
#include "argArray.h"

// globals ..
// why not just  class variables?
bool g_onlineFlag = true;

/************************************/
int g_sigma_cut_factor = 4;	// not used here 17/4/17
/************************************/
using namespace std;

template<typename EventReader, typename Source>
void ProcessEvents(Source, int nEvents);
int buildEventVectors(std::vector<unsigned int>& rawEvent);
void buildFrameContainer(std::vector<unsigned int>& rawEvent, std::list <CUDPData*>* m_datacontainer);
int prepareArg(char argvConfig[argRows][argColumns], char* pArg[], const char rfn[], const CmdArgStr pedFile,
               const CmdArgStr configFile, const int cModeDis);
void getGeometry(void);
void detectorGeometry(void);
void detectorGeometryBL4S(void);
void drawMicroMegasline(float x0, float y0, float z0,
                         float x1, float y1, float z1,
                         int color);
void init_mm( int argcConfig, char* pArg []);
void term_handler(int);

int 
main(int argc, char** argv)
{
  std::cout << " main: " << "argv[0] = " << argv[0] << std::endl;

  CmdArgStr partition_name ('p', "partition", "partition-name", "partition to work in." );
  CmdArgInt events         ('e', "events", "event-numbers", " number of events to display (default 1)\n");
  CmdArgInt jump           ('j', "jump", "jump-mode", "number of events to skip (default 0)\n");
  CmdArgInt projection     ('q', "projection", "projection-mode", "plot track projections (default 0)\n");
  CmdArgStr runType        ('r', "runType", "run-type", "run type: physics/pedestal");
  CmdArgStr datafilename   ('f', "datafilename", "data-file-name", "Data file name, read events from the file instead of online stream");
  CmdArgStr cfg_file_name  ('c', "cfg_file_name", "cfg_file_name", "BL4S config file name");
  CmdArgStr pedestalFile   ('s', "pedestalFile", "file-name", "MM pedestal file name");   
  CmdArgStr configFile     ('t', "configFile", "config-file", "MM configuration file name",CmdArg::isREQ);

//defaults
  partition_name = getenv("TDAQ_PARTITION");
  events = 1;
  jump = 0;
  projection = 0;
  runType = "physics";

  datafilename = "";
  cfg_file_name = "current_config.cfg";
  pedestalFile = "";
//  configFile = "";  MANDATORY

  CmdArgvIter arg_iter( --argc, ++argv );
  CmdLine cmd(*argv, &partition_name, &events, &jump, &projection, &datafilename,
                     &cfg_file_name, &runType, &pedestalFile, &configFile, NULL);
  cmd.description( "eventDisplay monitor program" );
  cmd.parse( arg_iter );

  m_jump = jump;
  m_projection = projection;
  std::cout << " m_projection = " << m_projection << std::endl;

  m_events = events;

/*****************************
  std::cout << " run type = " << string(runType) << std::endl;
  std::cout << " jump = " << m_jump << std::endl;
  std::cout << " datafilename = " << string(datafilename) << std::endl;
  std::cout << " len of datafilename = " << strlen(datafilename) << std::endl;
  std::cout << " len of pedestalFile = " << strlen(pedestalFile) << std::endl;
  std::cout << " configFile = " << string(configFile) << std::endl;
  std::cout << " len of config File = " << strlen(configFile) << std::endl;
**************************/

//  TApplication interferes with argc, argv : avoid same command definitions. See TApplication documentation.
// this should be AFTER the command line parsing above
  TApplication *myapp = new TApplication("EventDisplay", &argc, argv);

  if (strcmp(runType,"physics") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::CEvent::eventTypePhysics;
     m_preset_run_type = CConfiguration::runtypePhysics;
  }
  if (strcmp(runType,"pedestal") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::eventTypePedestals;
     m_preset_run_type = CConfiguration::runtypePedestals;;
  }

// prepare argc, argv for the MM configuration
  char argvConfig[argRows][argColumns] = {"Dummy"};
  char* pArg [10];	// array of pointers to argvConfig
  int argcConfig = prepareArg(argvConfig,pArg,"dummyRootFilename",pedestalFile,configFile,0);

  printf(" argcConfig = %d\n",argcConfig);
  for (int i=0; i<argcConfig; i++) {
    printf(" i = %d,  pArg[] = %s\n",i,pArg[i]);
  }

  std::string tmpString(cfg_file_name);
  m_cfg = new BL4SConfig(tmpString);
  m_cfg->ShowConfig();
  m_geo = new BL4SGeometry(m_cfg);
  m_geo->ShowGeometry();

// create the MM objects
  init_mm(argcConfig, pArg);

  try {
    IPCCore::init( argc, argv );
  } catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
  }

 signal(SIGTERM, term_handler);
  IPCPartition partition(partition_name);

  time(&m_run_start_time);	// start of THIS "ROOT run"

  m_chamberNames = m_config->defined_chamber_names(); // rename ..

  getGeometry();
//  detectorGeometry();
  detectorGeometryBL4S();

  int noScintillators = m_geo->GetNScintillator();
  if (noScintillators > 0) {
    for (int i = 0; i<noScintillators; i++) {
      double scXdim, scYdim, scZdim;
      m_geo->GetScintillatorDimension(i, scXdim, scYdim, scZdim);
      double scXpos, scYpos, scZpos;
      m_geo->GetScintillatorPosition(i, scXpos,scYpos,scZpos);
      m_scintDetector.push_back (new ScintillatorMon("SC" + std::to_string(i), // REVIEW
                                          1000.0*scXpos,1000.0*scYpos,1000.0*scZpos,    // m to mm
                                          1000.0*scXdim, 1000.0*scYdim, 1000.0*scZdim,
                                          1.0, 1.0));
    }
  }

  // create chambers , indexed by ii, NOT along axis
  for (int ii = 0; ii<m_chamberNames.size(); ii++) {
    microMegasMon * mmDet = new microMegasMon( m_chamberNames[ii],
                                               m_xPos[m_chamberNames[ii]],
                                               m_yPos[m_chamberNames[ii]],
                                               m_zPos[m_chamberNames[ii]],
                                               m_xLength[m_chamberNames[ii]],
                                               m_yLength[m_chamberNames[ii]],
                                               m_zLength[m_chamberNames[ii]],
                                               1.0, 1.0);
    m_mmDetector[m_chamberNames[ii]] = mmDet;

  }

  if (strlen(datafilename) == 0) {
   std::cout << " Online monitoring" << std::endl;
   g_onlineFlag = true;
   // skip eventNumber and process m_jump
   ProcessEvents<DAQEventReader, IPCPartition>(partition, m_events+m_jump);
  }
  else {
   std::cout << " From file monitoring" << std::endl;
   g_onlineFlag = false;
   // skip eventNumber and process m_jump
   ProcessEvents<FileEventReader, string>(string(datafilename), m_events+m_jump);
  }


  myapp->Run(kTRUE);
//  myapp->Run();

  delete m_cfg;
  delete m_geo;

  delete m_eventVectors;
  delete m_decoder;
  delete m_config;

  std::cout << " eventDisplay, going to exit" << std::endl;

  return 0;
}

template <typename EventReader, typename Source>
void
ProcessEvents(Source src, int nEvents) {

  vector<unsigned int> rawEvent;
  EventReader evReader(src);
  int eventCount = 0;

  TEveManager::Create(kTRUE);  // must create after each Quit Root !

  for (int ii = 0; ii<m_scintDetector.size(); ii++) {
    m_scintDetector[ii]->DrawDetector();
  }

  for (int ii = 0; ii<m_chamberNames.size(); ii++) {
    std::cout << " process: " << " name = "
              << m_mmDetector[m_chamberNames[ii]]->GetName() << std::endl;

    m_mmDetector[m_chamberNames[ii]]->DrawDetector();
  }

// nEvents = # events to jump + # events to display
  while (eventCount < nEvents || nEvents == -1) {		// event loop
    cout << "\n # Event No.: " << eventCount << "  events - to junp+display = " << nEvents << endl;
    rawEvent = evReader.GetNextRawEvent();

    for(const unsigned int& it : rawEvent) {
//      cout << boost::format("%08x") % it << " "; 
    }

// specific code for ONE event monitoring programs
    if ((g_onlineFlag == true) && (eventCount == 1)) break;
    if ((g_onlineFlag == false) && (eventCount < m_jump)) {   // start at event m_jump
      ++eventCount;
      continue;
    }

    int retBuild = buildEventVectors(rawEvent);
    if( retBuild != CEventVectors::RETURN_OK) {
      ++eventCount;
      continue;
    }

    unsigned int apv_evt =                        m_eventVectors->get_apv_evt();
    std::vector <unsigned int> apv_fec =  	m_eventVectors->get_apv_fec();
    std::vector <unsigned int> apv_id  =  	m_eventVectors->get_apv_id();
    std::vector <unsigned int> apv_ch  =  	m_eventVectors->get_apv_ch();
    std::vector <std::string>  mm_id = 		m_eventVectors->get_mm_id();
    std::vector <unsigned int> mm_readout =	m_eventVectors->get_mm_readout();
    std::vector <unsigned int> mm_strip	 =	m_eventVectors->get_mm_strip();
    std::vector <std::vector <short> > apv_q =	m_eventVectors->get_apv_q();
    std::vector <short> apv_qmax =                m_eventVectors->get_apv_qmax();
    std::vector <short> apv_tbqmax =              m_eventVectors->get_apv_tbqmax();
 
    std::cout << " apv_evt = " << apv_evt << std::endl;

    std::cout << " size of apv_fec = " << apv_fec.size() 
              << " fecNo[0] = " << apv_fec[0]  << std::endl;

    std::cout << " size of apv_id = " << apv_id.size()
              << " id[0] = " << apv_id[0]
              << " id[apv_id.size()-1] = " << apv_id[apv_id.size()-1]
              << std::endl;

    std::cout << " size of apv_ch = " << apv_ch.size()
              << " ch[0] = " << apv_ch[0]  << std::endl;

    std::cout << " size of mm_id = " << mm_id.size()
              << " mm_id[0] = " << mm_id[0]
              << " mm_id[mm_id.size()-1] = " << mm_id[mm_id.size()-1]  << std::endl;

    std::cout << " size of mm_readout = " << mm_readout.size()
              << " mm_readout[0] = " << mm_readout[0]
              << " mm_readout[mm_readout.size()-1] = " << mm_readout[mm_readout.size()-1]  << std::endl;

    std::cout << " size of mm_strip = " << mm_strip.size()
                << " mm_strip[0] = " << mm_strip[0]
                << " mm_strip[mm_strip.size()-1] = " << mm_strip[mm_strip.size()-1]  << std::endl;

    int apvCh = 0;
    int timeBin = 0;
    int timeBinMax = 0;
    string currentChamber = m_chamberNames[0];

    map<string,int> minStripChamber;
    map<string,int> maxStripChamber;
    map<string,int> apvChChamber;
    map<string,float> minChargeChamber;
    map<string,float> maxChargeChamber;

    for (int ii = 0; ii<m_chamberNames.size(); ii++) {
      minChargeChamber[m_chamberNames[ii]] = 3000.0;
      maxChargeChamber[m_chamberNames[ii]] = -100.0;
    }

    timeBinMax = apv_q[1].size();
//  std::cout << " timeBinMax = " << timeBinMax << std::endl;
//  std::cout << " currentChamber = " << currentChamber << std::endl;
    for (auto row = apv_q.begin(); row != apv_q.end(); ++row)
    {
// one apvChannel (27 timebins)
      for (auto col = row->begin(); col != row->end(); ++col)
      {
//    std::cout << apvCh << " " << timeBin << " " << *col << "  " ;
//    std::cout << "index = " << apvCh*timeBinMax + timeBin << " ";
//    std::cout << "mm_id = " << mm_id[apvCh]  <<  " ";

      if (mm_id[apvCh] != currentChamber) {
        currentChamber = mm_id[apvCh];
        cout << " now chamber " << currentChamber << endl;
      }
      int stripNo = mm_strip[apvCh];
//    std::cout << "apvChannel = " << apvCh << " ";
//    std::cout << "index = " << (apvCh - apvChannelFirst)*timeBinMax + timeBin << " ";
//    std::cout << stripNo << " " << timeBin << " " << *col << "  " ;
      if (*col > 3200 ) {
         std::cout << stripNo << " " << timeBin << " " << *col << "  " ;
      }
      if (*col > maxChargeChamber[currentChamber]) {
        maxChargeChamber[currentChamber] = *col;
        maxStripChamber[currentChamber] = mm_strip[apvCh];
        apvChChamber[currentChamber] = apvCh;
//        cout << " maxChargeChamber " << " chamber = " << mm_id[apvCh] <<
//                " maxCharge = " <<  maxChargeChamber[currentChamber] <<
//                " maxStrip = " <<  maxStripChamber[currentChamber] <<
//                " apv Channel = " << apvChChamber[currentChamber] <<  endl;
      }
         timeBin++;
    }
//       timeBinMax = timeBin;
       timeBin = 0;
       apvCh++;
    }

// REVIEW: command line parameter?
// global cut on Qmax
    const int globalCutQmax = 50;
    int missingHits = 0;
    for (int ii = 0; ii<m_chamberNames.size(); ii++) {
      if (maxChargeChamber[m_chamberNames[ii]] < globalCutQmax) {
        missingHits++;
        std::cout << " chamber name = " << m_chamberNames[ii]
                  << " no hits " << std::endl;
      }
    }
    if (missingHits > 0) {
      std::cout << " NOT all chambers hit " << std::endl;
      eventCount++;
      continue;
    }

// relative to middle of chamber:
// if the chamber is centered on the Z axis, this is also ABSOLUTE!
    float xyRel;
//   float xyRel0;
    float xyRelMiddle;// SETUP DEPENDENT
    float midStrip;
    int noHits = 0;
    for (int ii = 0; ii<m_chamberNames.size(); ii++) {
       cout << " xyRel " << " chamber name = " << m_chamberNames[ii] <<
               " maxCharge = " <<  maxChargeChamber[m_chamberNames[ii]] <<
               " maxStrip = " <<  maxStripChamber[m_chamberNames[ii]] <<
               " apv Channel = " << apvChChamber[m_chamberNames[ii]] << std::endl;
      float maxCharge = float(maxChargeChamber[m_chamberNames[ii]]);

      int relStripNo = maxStripChamber[m_chamberNames[ii]]-1;    // strip 1 corresponds to xyRel = 0
      xyRel = float(relStripNo) * m_pitch[m_chamberNames[ii]] - m_xLength[m_chamberNames[ii]]/2.0;
      std::cout << " Process : " << " chamber name = " << m_chamberNames[ii] 
                << " z = " << m_zPos[m_chamberNames[ii]] << " xy = " << xyRel << std::endl;
      m_xyRel[m_chamberNames[ii]] = xyRel;
    }
// separate X hits from Y hits, maps may have e size of 4,2 or 0
    for (int ii = 0; ii<m_chamberNames.size(); ii++) {
      if (m_angle[m_chamberNames[ii]] == 90.0) {
        m_xRel[m_chamberNames[ii]] = m_xyRel[m_chamberNames[ii]];
      }
      else if (m_angle[m_chamberNames[ii]] == 0.0) {
        m_yRel[m_chamberNames[ii]] = m_xyRel[m_chamberNames[ii]];
      }
      else {
        std::cout << " illegal angle, exit " << std::endl;
        exit(0);
      }
    }

    std::cout << "Process " << " size of m_xRel = " << m_xRel.size()
                           << " size of m_yRel = " << m_yRel.size() << std::endl;

    for (int ii = 0; ii<m_chamberNames.size(); ii++) {
      if (m_angle[m_chamberNames[ii]] == 90.0 ) {  // x-z
        m_mmDetector[m_chamberNames[ii]]->SetmicroMegasSignal(m_xRel[m_chamberNames[ii]], 0.0);
        m_mmDetector[m_chamberNames[ii]]->ShowmicroMegasSignal();
      }
      if (m_angle[m_chamberNames[ii]] == 0.0 ) {  // y-z
        m_mmDetector[m_chamberNames[ii]]->SetmicroMegasSignal(0.0, m_yRel[m_chamberNames[ii]]);
        m_mmDetector[m_chamberNames[ii]]->ShowmicroMegasSignal();
      }
    }
    if (m_xRel.size() == m_chamberNames.size()) {  // four parallel X chambers
      drawMicroMegasline(m_xRel[m_firstChamber],0.0,m_zPos[m_firstChamber],
                          m_xRel[m_lastChamber],0.0,m_zPos[m_lastChamber],
                          kYellow);
    }
    else if (m_xRel.size() == 2 && m_yRel.size() == 2) { // 2*X, 2*Y only configuration allowed

      float x0,x1,x2,x3,y0,y1,y2,y3,z0,z1,z2,z3;
      std::string chamber0, chamber1, chamber2,chamber3; // local numbering

      std::map <std::string,float>::iterator it = m_xRel.begin();
      x0 = it->second; 
      y0 = 0.0;
      chamber0 = it->first;
      z0 = m_zPos[chamber0];
      it = m_xRel.end();
      it--;  // last element !! end() -1
      x1 = it->second; 
      y1 = 0.0;
      chamber1 = it->first;
      z1 = m_zPos[it->first];
      std::cout << " Process "
                << " chamber " << chamber0 << " x0 = " << x0 << " y0 = " << y0 << " z0 = " << z0
                << " chamber " << chamber1 << " x1 = " << x1 << " y1 = " << y1 << " z1 = " << z1
                << std::endl;
      if (m_projection != 0) {
        drawMicroMegasline(x0, y0, z0, x1, y1, z1,kYellow);  // the x-z projection
      }
      it = m_yRel.begin();
      x2 = 0.0; 
      y2 = it->second;
      chamber2 = it->first;
      z2 = m_zPos[chamber2];
      it = m_yRel.end();
      it--;  // last element !! end() -1
      x3 = 0.0; 
      y3 = it->second;
      chamber3 = it->first;
      z3 = m_zPos[chamber3];
      std::cout << " Process "
                << " chamber " << chamber2 << " x2 = " << x2 << " y2 = " << y2 << " z2 = " << z2
                << " chamber " << chamber3 << " x3 = " << x3 << " y3 = " << y3 << " z3 = " << z3
                << std::endl;
      if (m_projection != 0) {
        drawMicroMegasline(x2, y2, z2, x3, y3, z3, kYellow);  // the y-z projection
      }
// extrapolate the y coordinates to the x chambers
// the slope calculation is independent of the z ordering of the chambers
      float aa = (y2 - y3)/(z2 - z3);
      float bb = y2 - aa*z2;  // or z3

// calculate y0,y1 above - were zero
      y0 = aa*z0 + bb; // X chambers
      y1 = aa*z1 + bb;

      std::cout << " Process " << " aa = " << aa << " b = " << bb
                << " y0 = " << y0 << " y1 = " << y1 << std::endl;
      if (m_projection != 0) {
        drawMicroMegasline(0.0, y0, z0, 0.0, y1, z1, kMagenta);  // the y-z projection at the Z position of the X chambers
      }
// for completeness, also extrapolate from  X to Y chambers
// then we x,y coordinates in al chambers
// the slope calculation is independent of the z ordering of the chambers
      aa = (x1 - x0)/(z1 - z0);
      bb = x0 - aa*z0;  // or z1

// calculate x2,x3 above - were zero
      x2 = aa*z2 + bb; // Y chambers
      x3 = aa*z3 + bb;

      std::cout << " Process " << " aa = " << aa << " b = " << bb
                << " x2 = " << x2 << " x3 = " << x3 << std::endl;
      if (m_projection != 0) {
        drawMicroMegasline(x2, 0.0, z2, x3, 0.0, z3, kMagenta);  // the x-z projection at the Z position of the X chambers
      }
// now we have all the space coordinates
// NOTE: this also pushes chamber names into m_xyz !
// in a loop this creates large vectors if NOT reset

     m_xyz[chamber0].push_back(x0);
     m_xyz[chamber0].push_back(y0);
     m_xyz[chamber0].push_back(z0);
     m_xyz[chamber1].push_back(x1);
     m_xyz[chamber1].push_back(y1);
     m_xyz[chamber1].push_back(z1);
     m_xyz[chamber2].push_back(x2);
     m_xyz[chamber2].push_back(y2);
     m_xyz[chamber2].push_back(z2);
     m_xyz[chamber3].push_back(x3);
     m_xyz[chamber3].push_back(y3);
     m_xyz[chamber3].push_back(z3);

     for (auto it = m_xyz.begin(); it != m_xyz.end(); ++it) {
       std::cout << " chamber = " << it->first << " x = " << it->second[0]
                 << " y = " << it->second[1] << " z = " << it->second[2] << std::endl;
     } 

// display the 3D track
     for (auto it = m_xyz.begin(); it != m_xyz.end(); ++it) {
        m_mmDetector[it->first]->SetmicroMegasSignal(it->second[0],it->second[1]);
        m_mmDetector[it->first]->ShowmicroMegasSignal();
     }
     drawMicroMegasline(m_xyz[m_firstChamber][0],m_xyz[m_firstChamber][1], m_xyz[m_firstChamber][2],
                        m_xyz[m_lastChamber][0],m_xyz[m_lastChamber][1], m_xyz[m_lastChamber][2],
                        kGreen);

// clear event data
     m_xyz[chamber0].clear();
     m_xyz[chamber1].clear();
     m_xyz[chamber2].clear();
     m_xyz[chamber3].clear();

    }
    else {
      std::cout << " Process" << " illegal configuration" << std::endl;
      exit(0);
    }

     ++eventCount;
     cout << "\n" << endl;
  } // event loop
}

void init_mm( int argcConfig, char* pArg []) {

// construct MM objects: config, decoder, writer and event vectors
  try {
    m_config = new CConfiguration(argcConfig, pArg, "dummy"); // config file name is in pArg
  }
  catch (std::string str) {
     throw runtime_error(string("Errors in Configuration: ") + str);
  }
  if (m_config->error()) {
     throw runtime_error("Other Errors in configuration: ");
  }

  m_save_data_flag = true;             // REVIEW

  m_config->run_type(m_preset_run_type);

  m_decoder = new CEventDecoder(m_config, &m_datacontainer, &m_eventcontainer);
//  m_decoder->attach(m_receiverFile);

  m_decoder->preset_event_type(m_preset_event_type);

  m_eventVectors = new CEventVectors(m_config, &m_eventcontainer, m_save_data_flag);

  std::cout << " eventDisplay, CEventVectors OK " << std::endl;

}

void 
term_handler(int param) {
  assert(param == 15);
  std::cout << "Handling SIGTERM... exiting" << std::endl;
  exit(0);
}

int buildEventVectors(std::vector<unsigned int>& rawEvent) {

  int err = 0;

  buildFrameContainer(rawEvent,&m_datacontainer );

  err = m_decoder->buildEventContainer();
  std::cout << " ProcessEvents" << " after buildEventContainer, err = " << err << std::endl;

  std::cout << " ProcessEvents " << " size of event container = " << m_eventcontainer.size() << std::endl;

  err = m_eventVectors->buildVectors();

  std::cout << " ProcessEvents" << " after buildVectors(), err = " << err << std::endl;

  return err;

}

int getMaxStrips(CmdArgStr chamberName) {

  TEnv* tenv = (TEnv*)m_config->get_tenv_config();
  stringstream ss;
  ss.str("");
  ss << "mmdaq.Chamber." << chamberName << ".Strips.X.Max";
//   cout << " ss = " << ss.str() << endl;
//   cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
  string str1 = tenv->GetValue(ss.str().c_str(), "");
  int maxStrips = stoi(str1);
  cout << " getMaxStrips:  chamber name = " << chamberName << " max strips = "  << maxStrips << endl;
  return maxStrips;
}

// line between hits  - a local function
void drawMicroMegasline(float x0, float y0, float z0,
                         float x1, float y1, float z1,
                         int color)
{
  TEveLine *l = new TEveLine(2);
  l->SetPoint(0,x0, y0, z0);
  l->SetPoint(1,x1, y1, z1);

  l->SetMainColor(color);
  gEve->AddElement(l);

}

void getGeometry(void) {

// homebuilt way of getting geometry of chambers; see recomm
   float maxStrips;

   TEnv* tenv = (TEnv*)m_config->get_tenv_config();
   for (int ii = 0; ii<m_chamberNames.size(); ii++) {
     stringstream ss;
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".ZPos";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     string str1 = tenv->GetValue(ss.str().c_str(), "");
//     cout << " str1= " << str1 << endl;
     m_zPos[m_chamberNames[ii]] = stof(str1);
     cout << " getGeometry " << "  chamber name = " << m_chamberNames[ii]
          << " zPos = "  << m_zPos[m_chamberNames[ii]] << endl;

     ss.str("");
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".Strips.X.Angle";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     str1 = tenv->GetValue(ss.str().c_str(), "");
     m_angle[m_chamberNames[ii]] = stof(str1);
     cout << " getGeometry " << "  chamber name = " << m_chamberNames[ii]
          << " angle = "  << m_angle[m_chamberNames[ii]] << endl;

     ss.str("");
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".Strips.X.Pitch";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     str1 = tenv->GetValue(ss.str().c_str(), "");
     m_pitch[m_chamberNames[ii]] = stof(str1);
     cout << " getGeometry " << "  chamber name = " << m_chamberNames[ii]
          << " pitch = "  << m_pitch[m_chamberNames[ii]] << endl;

     ss.str("");
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".Strips.X.Max";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     str1 = tenv->GetValue(ss.str().c_str(), "");
     maxStrips = stof(str1);
     cout << " getGeometry " << "  chamber name = " << m_chamberNames[ii]
          << " max strip = "  << maxStrips << endl;
     m_maxStrips[m_chamberNames[ii]] = maxStrips; // 1024
   }
}


// 2/3/2018  becoming obsolete ..
// compute chamber position and dimension for display based on MM configuration
void detectorGeometry(void) {
   float noGaps;
   for (int ii = 0; ii<m_chamberNames.size(); ii++) {
     noGaps = m_maxStrips[m_chamberNames[ii]] - 1.0;
     m_xLength[m_chamberNames[ii]] = noGaps * m_pitch[m_chamberNames[ii]];
//     m_xPos[m_chamberNames[ii]] = m_xLength[m_chamberNames[ii]] / 2.0;
     m_xPos[m_chamberNames[ii]] = 0.0;    // on beam axis
     m_yLength[m_chamberNames[ii]] = m_xLength[m_chamberNames[ii]];  // REVIEW
     m_yPos[m_chamberNames[ii]] = m_xPos[m_chamberNames[ii]];
     m_zLength[m_chamberNames[ii]] = 5.0;  // mm, arbitrary
     std::cout << " chamber name = " << m_chamberNames[ii] <<
                                   " m_xLength  = " << m_xLength[m_chamberNames[ii]] <<
                                   " m_xPos    = " << m_xPos[m_chamberNames[ii]] << 
                                   " m_yLength  = " << m_yLength[m_chamberNames[ii]] <<
                                   " m_yPos    = " << m_yPos[m_chamberNames[ii]] <<
                                   " m_zLength  = " << m_zLength[m_chamberNames[ii]] <<
                                   " m_zPos    = " << m_zPos[m_chamberNames[ii]] <<  std::endl;
   }

}

// compute chamber position and dimension for display based on BL4S geometry file
void detectorGeometryBL4S(void) {

   std::vector <std::string> chName = {"BL4Smm1","BL4Smm2","BL4Smm3","BL4Smm4"};

// ONLY chambers with APVs - this comes from the MM config
   for (int ii = 0; ii<m_chamberNames.size(); ii++) {
     double xl, yl, zl;
     m_geo->GetMmDimension(xl, yl, zl);

// this is chamber independent ..
     m_xLength[m_chamberNames[ii]] = 1000.0*(float)xl;  // mm
     m_yLength[m_chamberNames[ii]] = 1000.0*(float)yl;
     m_zLength[m_chamberNames[ii]] = 1000.0*(float)zl;

     double xPos, yPos, zPos;
     m_geo->GetMmPosition(ii, xPos, yPos, zPos);

// loop over cfg names - pick up the x,y,z for m_chamberNames[ii]
     for (int jj = 0; jj<chName.size(); jj++) {
       if (chName[ii] == m_chamberNames[ii]) {
         m_xPos[chName[ii]] = 1000.0*(float)xPos;
         m_yPos[chName[ii]] = 1000.0*(float)yPos;
         m_zPos[chName[ii]] = 1000.0*(float)zPos;
       }
     }

     std::cout << " chamber name = " << m_chamberNames[ii] <<
                                   " m_xLength  = " << m_xLength[m_chamberNames[ii]] <<
                                   " m_xPos    = " << m_xPos[m_chamberNames[ii]] << 
                                   " m_yLength  = " << m_yLength[m_chamberNames[ii]] <<
                                   " m_yPos    = " << m_yPos[m_chamberNames[ii]] <<
                                   " m_zLength  = " << m_zLength[m_chamberNames[ii]] <<
                                   " m_zPos    = " << m_zPos[m_chamberNames[ii]] <<  std::endl;
   }
 
// Google 'max_element map second element' stackoverflow  
// default is to compare first element, the name, lexically
   std::pair<std::string,float> firstCh = *std::min_element(m_zPos.begin(),m_zPos.end(),
                                [](const pair<std::string, float>& p1, const pair<std::string, float>& p2)
                                 { return p1.second < p2.second; });
   std::cout << " detectorGeometryBL4S " <<  " first chamber = " << firstCh.first << std::endl;
   std::cout << " detectorGeometryBL4S " <<  " first position = " << firstCh.second << std::endl;
   m_firstChamber = firstCh.first;

   std::pair<std::string,float> lastCh = *std::max_element(m_zPos.begin(),m_zPos.end(),
                                [](const pair<std::string, float>& p1, const pair<std::string, float>& p2)
                                 { return p1.second < p2.second; });
   std::cout << " detectorGeometryBL4S " <<  " last chamber = " << lastCh.first << std::endl;
   std::cout << " detectorGeometryBL4S " <<  " last position = " << lastCh.second << std::endl;
   m_lastChamber = lastCh.first;

}
