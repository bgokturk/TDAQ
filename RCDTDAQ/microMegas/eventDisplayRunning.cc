/**************************************************/
/*  Event Display 			          */
/*  runs as a TApplication			  */
/*  running display that only plots 2D histogram   */
/*  a subset of eventDisplay                       */
/*                                                */
/*  author:  J.Petersen 		          */
/*  2017/05/06					  */
/*  2017/08/08  display all chambers		  */
/*  adapt to zero suppression Jan. 2018           */
/**************************************************/

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <boost/format.hpp>

#include <time.h>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include "DAQEventReader.h"
#include "FileEventReader.h"

// MM classes
#include "CConfiguration.h"
#include "CEventDecoder.h"
#include "CEventVectors.h"
#include "CUDPData.h"
#include "CEvent.h"

#include <TEnv.h>
#include <TString.h>
#include <TApplication.h>
#include "TCanvas.h"
#include "TH2I.h"

#include "eventDisplay.h"
#include "argArray.h"

using namespace std;

// globals ..
// why not just  class variables?
bool g_onlineFlag = true;
bool g_pedestalSubtraction;
int g_deltaTime;

map <string,TCanvas*> g_canvas;
map <string,TH2I*> g_apvQ;

template<typename EventReader, typename Source>
void ProcessEvents(Source, int nEvents);
int buildEventVectors(std::vector<unsigned int>& rawEvent);
void buildFrameContainer(std::vector<unsigned int>& rawEvent, std::list <CUDPData*>* m_datacontainer);
int prepareArg(char argvConfig[argRows][argColumns], char* pArg[], const char rfn[], const CmdArgStr pedFile,
               const CmdArgStr configFile, const int cModeDis);
void init_mm( int argcConfig, char* pArg []);
void plotHistos ();
void resetHistos ();
int getMaxStrips(string chStr);
void term_handler(int);

int 
main(int argc, char** argv)
{
  std::cout << " main: " << "argv[0] = " << argv[0] << std::endl;

  CmdArgStr partition_name ('p', "partition", "partition-name", "partition to work in." );
  CmdArgInt events         ('e', "events", "events-number", "number of events to retrieve (default 1)\n"
                                                         "-1 means work forever" );
  CmdArgStr runType        ('r', "runType", "run-type", "run type: physics/pedestal");

  CmdArgStr datafilename   ('f', "datafilename", "data-file-name", "Data file name, read events from the file instead of online stream");
   
  CmdArgStr pedestalFile   ('s', "pedestalFile", "file-name", "MM pedestal file name");
  CmdArgStr configFile     ('t', "configFile", "config-file", "MM configuration file name",CmdArg::isREQ);
  CmdArgInt deltaTime      ('d', "deltaTime", "delta-time", " time between histogram updates (seconds)");

//defaults
  partition_name = getenv("TDAQ_PARTITION");
  events = -1;
  runType = "physics";

  datafilename = "";
  pedestalFile = "";
  deltaTime = 5;

  CmdArgvIter arg_iter( --argc, ++argv );
  CmdLine cmd(*argv, &partition_name, &events, &datafilename, &runType, 
                     &pedestalFile, &configFile, &deltaTime, NULL);
  cmd.description( "eventDisplay monitor program" );
  cmd.parse( arg_iter );

  g_deltaTime = deltaTime;

/*****************************
  std::cout << " run type = " << string(runType) << std::endl;
  std::cout << " datafilename = " << string(datafilename) << std::endl;
  std::cout << " len of datafilename = " << strlen(datafilename) << std::endl;
  std::cout << " pedestalFile = " << string(pedestalFile) << std::endl;
  std::cout << " len of pedestalFile = " << strlen(pedestalFile) << std::endl;
  std::cout << " configFile = " << string(configFile) << std::endl;
  std::cout << " len of config File = " << strlen(configFile) << std::endl;
  std::cout << " zsEnable = " << zsEnable << std::endl;
**************************/

//  TApplication interferes with argc, argv : avoid same command definitions. See TApplication documentation.
// this should be AFTER the command line parsing above
  TApplication *myapp = new TApplication("EventDisplay", &argc, argv);

  if (strcmp(runType,"physics") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::CEvent::eventTypePhysics;
     m_preset_run_type = CConfiguration::runtypePhysics;
  }
  if (strcmp(runType,"pedestal") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::eventTypePedestals;
     m_preset_run_type = CConfiguration::runtypePedestals;;
  }

// prepare argc, argv for the MM configuration
  char argvConfig[argRows][argColumns] = {"Dummy"};
  char* pArg [10];	// array of pointers to argvConfig
  int argcConfig = prepareArg(argvConfig,pArg,"dummyRootFilename",pedestalFile,configFile,0);

  printf(" argcConfig = %d\n",argcConfig);
  for (int i=0; i<argcConfig; i++) {
    printf(" i = %d,  pArg[] = %s\n",i,pArg[i]);
  }

  try {
    // create the MM objects
    init_mm(argcConfig, pArg);

  
    // initialize TDAQ IPC
    IPCCore::init( argc, argv );
  } 
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
  }
  catch ( runtime_error &ex ) {
    cerr << ex.what() << endl;
    return 1;
  }

 signal(SIGTERM, term_handler);
  IPCPartition partition(partition_name);

  time(&m_run_start_time);	// start of THIS "ROOT run"

  int cc = 0;
  for (auto chamberName: m_config->defined_chamber_names()){
    TCanvas* canvas = new TCanvas(("canvas" + chamberName).c_str(),
                                  ("c" + chamberName).c_str(),
                                   200+cc*100,200+cc*100,700,500);
    g_canvas[chamberName] = canvas;
    cc++;
    m_maxStrips = getMaxStrips(chamberName);  // same for all chambers ..
  }

  if (strlen(datafilename) == 0) {
   std::cout << " Online monitoring" << std::endl;
   g_onlineFlag = true;
   ProcessEvents<DAQEventReader, IPCPartition>(partition, events);
  }
  else {
   std::cout << " From file monitoring" << std::endl;
   g_onlineFlag = false;
   ProcessEvents<FileEventReader, string>(string(datafilename), events);
  }

//  plotHistos();

//  myapp->Run(kTRUE);   // segfaults after exit ??
  myapp->Run();         // needed for interactivity: to get ROOT tool panel

  delete m_eventVectors;
  delete m_decoder;
  delete m_config;

  std::cout << " eventDisplay, going to exit" << std::endl;

  return 0;
}

template <typename EventReader, typename Source>
void
ProcessEvents(Source src, int events) {

  vector<unsigned int> rawEvent;
  EventReader evReader(src);
  bool firstTime = true;

  g_pedestalSubtraction = !(string(m_config-> pedestal_filename()).empty());
  cout << "ProcessEvents" << " g_pedestalSubtraction = " << g_pedestalSubtraction << endl;

  cout << "ProcessEvents" << "Number of events: " << events << endl;
  int eventCount = 0;
  while (eventCount < events || events == -1) {		// event loop
    cout << "\n # Event No.: " << eventCount << " events argument = " << events << endl;
    rawEvent = evReader.GetNextRawEvent();

  int apv_ptr = 9 ;  // RODHDRSIZE 
  std::string ap(1,(char)((rawEvent[apv_ptr+3] & 0x00ff0000)>>16));
  if (ap == "Z") {
    m_zsEnable = 1;   // not used at present
    std::cout << " ZS mode " << std::endl;
  }

//    for(const unsigned int& it : rawEvent) {
//      cout << boost::format("%08x") % it << " "; 
//    }

  int retBuild = buildEventVectors(rawEvent);
  if( retBuild != CEventVectors::RETURN_OK) {
    continue;
  }


  unsigned int apv_evt =                        m_eventVectors->get_apv_evt();
  std::vector <unsigned int> apv_fec =  	m_eventVectors->get_apv_fec();
  std::vector <unsigned int> apv_id  =  	m_eventVectors->get_apv_id();
  std::vector <unsigned int> apv_ch  =  	m_eventVectors->get_apv_ch();
  std::vector <std::string>  mm_id = 		m_eventVectors->get_mm_id();
  std::vector <unsigned int> mm_readout =	m_eventVectors->get_mm_readout();
  std::vector <unsigned int> mm_strip	 =	m_eventVectors->get_mm_strip();
  std::vector <std::vector <short> > apv_q =	m_eventVectors->get_apv_q();
  std::vector <short> apv_qmax =                m_eventVectors->get_apv_qmax();
  std::vector <short> apv_tbqmax =              m_eventVectors->get_apv_tbqmax();
 
/*********************************************************
  std::cout << " apv_evt = " << apv_evt << std::endl;

  std::cout << " size of apv_fec = " << apv_fec.size() 
            << " fecNo[0] = " << apv_fec[0]  << std::endl;

  std::cout << " size of apv_id = " << apv_id.size()
            << " id[0] = " << apv_id[0]
            << " id[apv_id.size()-1] = " << apv_id[apv_id.size()-1]
            << std::endl;

  std::cout << " size of apv_ch = " << apv_ch.size()
            << " ch[0] = " << apv_ch[0]  << std::endl;

  std::cout << " size of mm_id = " << mm_id.size()
            << " mm_id[0] = " << mm_id[0]
            << " mm_id[mm_id.size()-1] = " << mm_id[mm_id.size()-1]  << std::endl;

  std::cout << " size of mm_readout = " << mm_readout.size()
            << " mm_readout[0] = " << mm_readout[0]
            << " mm_readout[mm_readout.size()-1] = " << mm_readout[mm_readout.size()-1]  << std::endl;

  std::cout << " size of mm_strip = " << mm_strip.size()
              << " mm_strip[0] = " << mm_strip[0]
              << " mm_strip[mm_strip.size()-1] = " << mm_strip[mm_strip.size()-1]  << std::endl;

*********************************************************/

  if (firstTime) {
    // create strip to channel MAP for ONE chamber
    // NB: in ZS mode, the stripChannel map may be empty for this chamber
    for (auto chamberName: m_config->defined_chamber_names()){
      for (int jj = 0; jj<mm_strip.size(); jj++) {  // jj = apvChannel
        if (mm_id[jj] == chamberName) {
          int strip = mm_strip[jj];
//          std::cout << " apvChannel = "  << jj << " strip = " << strip << std::endl;
          m_stripToChannelChamber[strip] =  jj;
        }
      }
      m_stripToChannel.push_back(m_stripToChannelChamber);
    }
  }

  int apvCh = 0;
  int timeBin = 0;
  int timeBinMax = 0;

  timeBinMax = apv_q[1].size();
//  std::cout << " timeBinMax = " << timeBinMax << std::endl;

// define histos when dimensions known 
  if (firstTime) {
    for (auto chamberName: m_config->defined_chamber_names()){
      g_apvQ[chamberName] = new TH2I(("apvQ"+chamberName).c_str(),
             (" Chamber "  + chamberName + " Charge of all APV measurements").c_str(),
              m_maxStrips,1,m_maxStrips+1,
              timeBinMax, 0, timeBinMax);
      g_apvQ[chamberName]->GetXaxis()->SetTitle("strip number");
      g_apvQ[chamberName]->GetYaxis()->SetTitle("timeBin(25ns)");
      g_apvQ[chamberName]->GetZaxis()->SetTitle("charge");
    }
    firstTime = false;
  }

  for (auto row = apv_q.begin(); row != apv_q.end(); ++row)
  {

// one apvChannel (27 timebins)
       for (auto col = row->begin(); col != row->end(); ++col)
       {
//         std::cout << apvChannel << " " << timeBin << " " << *col << "  " ;
//         std::cout << "index = " << apvCh*timeBinMax + timeBin << " ";
//         std::cout << "mm_id = " << mm_id[apvCh]  <<  " ";

           int stripNo = mm_strip[apvCh];
           string chName = mm_id[apvCh];
//           std::cout << "apvChannel = " << apvCh << " ";
//           std::cout << " chName = " << chName << " strip no = " << stripNo
//                     << " " << timeBin << " " << *col << "  " ;
           g_apvQ[chName]->Fill(stripNo, timeBin, *col);

           timeBin++;
       }
//       timeBinMax = timeBin;
       timeBin = 0;
       apvCh++;
   }

   plotHistos();
   resetHistos();
   sleep(g_deltaTime);

   ++eventCount;
   cout << "\n" << endl;
 }
}

void init_mm( int argcConfig, char* pArg []) {

// construct MM objects: config, decoder, writer and event vectors
  try {
    m_config = new CConfiguration(argcConfig, pArg, "dummy");
  }
  catch (std::string str) {
     throw runtime_error(string("Errors in Configuration: ") + str);
  }
  if (m_config->error()) {
     throw runtime_error("Other Errors in configuration: ");
  }

  m_save_data_flag = true;             // REVIEW

  m_config->run_type(m_preset_run_type);

  m_decoder = new CEventDecoder(m_config, &m_datacontainer, &m_eventcontainer);
//  m_decoder->attach(m_receiverFile);

  m_decoder->preset_event_type(m_preset_event_type);

  m_eventVectors = new CEventVectors(m_config, &m_eventcontainer, m_save_data_flag);

  std::cout << " eventDisplay, CEventVectors OK " << std::endl;

}

void 
term_handler(int param) {
  assert(param == 15);
  std::cout << "Handling SIGTERM... exiting" << std::endl;
  exit(0);
}

int buildEventVectors(std::vector<unsigned int>& rawEvent) {

  int err = 0;

  buildFrameContainer(rawEvent,&m_datacontainer );

  err = m_decoder->buildEventContainer();
  std::cout << " ProcessEvents" << " after buildEventContainer, err = " << err << std::endl;

  std::cout << " ProcessEvents " << " size of event container = " << m_eventcontainer.size() << std::endl;

  err = m_eventVectors->buildVectors();

  std::cout << " ProcessEvents" << " after buildVectors(), err = " << err << std::endl;

  return err;
}

// cannot reset a graph??
void resetHistos () {
  for (auto chamberName: m_config->defined_chamber_names()){
    g_apvQ[chamberName]->Reset();
  }
}

int getMaxStrips(string chamberName) {

  TEnv* tenv = (TEnv*)m_config->get_tenv_config();
  stringstream ss;
  ss.str("");
  ss << "mmdaq.Chamber." << chamberName << ".Strips.X.Max";
//   cout << " ss = " << ss.str() << endl;
//   cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
  string str1 = tenv->GetValue(ss.str().c_str(), "");
  int maxStrips = stoi(str1);
  cout << " getMaxStrips:  chamber name = " << chamberName << " max strips = "  << maxStrips << endl;
  return maxStrips;
}


void plotHistos () {

  std::cout << " plotHistos " << std::endl;

  for (auto chamberName: m_config->defined_chamber_names()){
    g_canvas[chamberName]->cd();
    g_apvQ[chamberName]->GetXaxis()->SetTitleOffset(1.5);
    g_apvQ[chamberName]->GetYaxis()->SetTitleOffset(1.5);
//  g_apvQ[chamberName]->Draw("LEGO");
    g_apvQ[chamberName]->Draw("SURF1");
    g_canvas[chamberName]->Update();
  }

}
