
#include "TRandom3.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TEveViewer.h"
#include "TEveBrowser.h"
#include "TEveText.h"
#include "TEveTrans.h"
#include "TGLViewer.h"

#include "BL4SMonitorDetectors.h"

BL4SBoxDetector::BL4SBoxDetector(std::string nname,
                                 double xx, double yy, double zz,
                                 double pwidth, double pheight, double plength,
                                 double pxyScale, double pzScale,
                                 int pcolor)
{
  m_name = nname;
  m_z = zz;
  m_y = yy;
  m_x = xx;
  m_color = pcolor;

//  rotation = beamline->GetDetectorRotation(z);
  m_rotation = 0;   // BL4S 2018
  // next line modifies x,z. Seems to be buggy: inverts x with angle = 0
  // comment out, we are not using rotations
//  beamline->GetDetectorXZPosition(x,z);
  m_width = pwidth;
  m_height = pheight;
  m_length = plength;

  m_xyScale = pxyScale;
  m_zScale = pzScale;
}

BL4SBoxDetector::~BL4SBoxDetector(){}

void BL4SBoxDetector::ShowInfo()
{
  cout << " name = " << m_name << std::endl;
  cout << "x: " << m_x << ", y: " << m_y << ", z: " << m_z << endl;
  cout << "width: " << m_width << ", length: " << m_length << ", height: " << m_height << endl;
  cout << "XY scale factor: " << m_xyScale << ", Z scale factor: " << m_zScale << endl;
  cout << "Rotation: " << m_rotation << endl;
}

void BL4SBoxDetector::DrawDetector() {

   string myName = m_name;
   double A = m_width;
   double B = m_height;
   double C = m_length;

   double theta = m_rotation; // * TMath::Pi() / 180.;
   double xx = m_x;
   double yy = m_y;
   double zz = m_z;
   int obj_color = m_color;
cout << " DrawDetector : " <<  " name = " << m_name
     << "  " <<   A << "  " << B << "  " << C << "  " << theta << endl;
cout << " DrawDetector x,y,z : " << xx << "  " << yy << "  " << zz << endl;
   //TEveManager::Create();
   TEveBox* b = new TEveBox;
   b->SetMainColor(obj_color);
   b->SetMainTransparency(60);

// draw detector around the CENTRE of the box
   //front of the detector
   b->SetVertex(0, -A/2, -B/2, -C/2 );
   b->SetVertex(3, -A/2, +B/2, -C/2 );
   b->SetVertex(1, +A/2, -B/2, -C/2 );
   b->SetVertex(2, +A/2, +B/2, -C/2 );

   //back of the detector
   //A++; B++;
   b->SetVertex(4, -A/2, -B/2, +C/2 );
   b->SetVertex(7, -A/2, +B/2, +C/2 );
   b->SetVertex(5, +A/2, -B/2, +C/2 );
   b->SetVertex(6, +A/2, +B/2, +C/2 );

   TGeoRotation *rot = new TGeoRotation("rot",0.,0.,0.);
   rot->SetAngles(90,theta,0);          // no effect on symmetrical XY objects IF theta == 0: rotate around z axis (why?)

   TGeoGenTrans det_trans(m_xyScale*xx, m_xyScale*yy, zz, m_xyScale, m_xyScale, m_zScale, rot); // scale BOTH position and dimension
   b->SetTransMatrix(det_trans);

   gEve->AddGlobalElement(b);

   std::cout << " T name = " << TString(m_name) << std::endl;

   TEveText* t = new TEveText(TString(m_name));
   t->PtrMainTrans()->SetPos(xx, yy, zz);
   gEve->AddElement(t);

   gEve->Redraw3D(kTRUE);
}

ScintillatorMon::ScintillatorMon(std::string name,
                                 double xx, double yy, double zz,
                                 double xdim, double ydim, double zdim,
                                 double xyScale, double zScale)
                : BL4SBoxDetector(name,xx,yy,zz,
                                  xdim,ydim,zdim,
                                  xyScale, zScale,
                                  kRed)
{ }

ScintillatorMon::~ScintillatorMon(){ }


microMegasMon::microMegasMon(std::string name,
                           double xx, double yy, double zz,
                           double xdim, double ydim, double zdim,
                           double xyScale, double zScale)
              : BL4SBoxDetector(name,xx,yy,zz,
                                xdim,ydim,zdim,
                                xyScale, zScale,
                                kCyan)
{ }

microMegasMon::~microMegasMon() {}

void microMegasMon::ShowmicroMegasSignal()
{
    TGeoRotation id;

    //TVector3 o(xs, ys, z);
    TEveGeoShape* det_shape = new TEveGeoShape();
    det_shape->IncDenyDestroy();
//    TGeoSphere* sphere = new TGeoSphere(0.,0.10);      // review: radius already scaled
    TGeoSphere* sphere = new TGeoSphere(0.,5.0);      // review: radius already scaled
    det_shape->SetShape(sphere);
    std::cout << " ShowmicroMegasSignal: position of hit = " << m_xs << "   " << m_ys << "   " << m_z  << std::endl;
    TGeoGenTrans det_trans(m_xyScale*m_xs,m_xyScale*m_ys,m_z,1.0,1.0,1.0, &id);     // REVIEW
    det_shape->SetTransMatrix(det_trans);

    det_shape->SetMainColor(kRed);
    det_shape->SetMainTransparency(0);
    gEve->AddElement(det_shape);

}
