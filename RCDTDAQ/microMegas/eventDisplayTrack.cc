/**************************************************/
/* Event Display  of Track with Fit	          */
/* runs as a TApplication			  */
/*                                                */
/*  author:  J.Petersen 		          */
/*  2017/07/14					  */
/**************************************************/

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <boost/format.hpp>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include "DAQEventReader.h"
#include "FileEventReader.h"

// MM classes
#include "CConfiguration.h"
#include "CEventDecoder.h"
#include "CEventVectors.h"
#include "CUDPData.h"
#include "CEvent.h"

#include <TString.h>
#include <TApplication.h>
#include "TCanvas.h"
#include "TGraph.h"
#include <TEnv.h>
#include <TFitResult.h>
#include <TStyle.h>
#include <TAxis.h>

#include "eventDisplayTrack.h"
#include "argArray.h"

#define PI 3.14159265

// globals ..
bool g_onlineFlag = true;
bool g_pedestalSubtraction;
std::map<int, double>* g_pedestals_stdev;   // ONLY if pedestal subtraction

TString g_chamberNameTstr;
/************************************/
int g_sigma_cut_factor = 4;	// not used here 17/4/17
/************************************/
TCanvas* g_canvas0;
TGraph* g_track;

using namespace std;

template<typename EventReader, typename Source>
void ProcessEvents(Source, int nEvents);
int buildEventVectors(std::vector<unsigned int>& rawEvent);
void buildFrameContainer(std::vector<unsigned int>& rawEvent, std::list <CUDPData*>* m_datacontainer);
int prepareArg(char argvConfig[argRows][argColumns], char* pArg[], const char rfn[], const CmdArgStr pedFile,
               const CmdArgStr configFile, const int cModeDis);
void getGeometry(void);
void init_mm( int argcConfig, char* pArg []);
void plotHistos ();
void term_handler(int);

int 
main(int argc, char** argv)
{
  std::cout << " main: " << "argv[0] = " << argv[0] << std::endl;

  CmdArgStr partition_name ('p', "partition", "partition-name", "partition to work in." );
  CmdArgInt events         ('e', "events", "events-number", "number of events to retrieve (default 1)\n"
                                                         "-1 means work forever" );
  CmdArgStr runType        ('r', "runType", "run-type", "run type: physics/pedestal");

  CmdArgStr datafilename   ('f', "datafilename", "data-file-name", "Data file name, read events from the file instead of online stream");
   
  CmdArgStr pedestalFile   ('s', "pedestalFile", "file-name", "MM pedestal file name");
  CmdArgStr configFile     ('t', "configFile", "config-file", "MM configuration file name",CmdArg::isREQ);
  CmdArgInt cmModeDis      ('u', "commonModeDisabled", "common-mode-disabled", "MM disable common mode");

//defaults
  partition_name = getenv("TDAQ_PARTITION");
  events = 1;
  runType = "physics";

  datafilename = "";
  pedestalFile = "";
  cmModeDis = 0;

  CmdArgvIter arg_iter( --argc, ++argv );
  CmdLine cmd(*argv, &partition_name, &events, &datafilename, &runType, 
                     &pedestalFile, &configFile, &cmModeDis, NULL);
  cmd.description( "eventDisplay monitor program" );
  cmd.parse( arg_iter );

/*****************************
  std::cout << " run type = " << string(runType) << std::endl;
  std::cout << " datafilename = " << string(datafilename) << std::endl;
  std::cout << " len of datafilename = " << strlen(datafilename) << std::endl;
  std::cout << " pedestalFile = " << string(pedestalFile) << std::endl;
  std::cout << " len of pedestalFile = " << strlen(pedestalFile) << std::endl;
  std::cout << " configFile = " << string(configFile) << std::endl;
  std::cout << " len of config File = " << strlen(configFile) << std::endl;
  std::cout << " cmModeDis = " << cmModeDis << std::endl;
**************************/

//  TApplication interferes with argc, argv : avoid same command definitions. See TApplication documentation.
// this should be AFTER the command line parsing above
  TApplication *myapp = new TApplication("EventDisplay", &argc, argv);

  if (strcmp(runType,"physics") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::CEvent::eventTypePhysics;
     m_preset_run_type = CConfiguration::runtypePhysics;
  }
  if (strcmp(runType,"pedestal") == 0) {
  std::cout << " run type = " << string(runType) << std::endl;
     m_preset_event_type = CEvent::eventTypePedestals;
     m_preset_run_type = CConfiguration::runtypePedestals;;
  }

// prepare argc, argv for the MM configuration
  char argvConfig[argRows][argColumns] = {"Dummy"};
  char* pArg [10];	// array of pointers to argvConfig
  int argcConfig = prepareArg(argvConfig,pArg,"dummyRootFilename",pedestalFile,configFile,cmModeDis);

  printf(" argcConfig = %d\n",argcConfig);
  for (int i=0; i<argcConfig; i++) {
    printf(" i = %d,  pArg[] = %s\n",i,pArg[i]);
  }

  try {
    // create the MM objects
    init_mm(argcConfig, pArg);
  
    // initialize TDAQ IPC
    IPCCore::init( argc, argv );
  } 
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
  }
  catch ( runtime_error &ex ) {
    cerr << ex.what() << endl;
    return 1;
  }

 signal(SIGTERM, term_handler);
  IPCPartition partition(partition_name);

  time(&m_run_start_time);	// start of THIS "ROOT run"

  g_canvas0 = new TCanvas("canvas0", "c0", 300,300,700,500);

  if (strlen(datafilename) == 0) {
   std::cout << " Online monitoring" << std::endl;
   g_onlineFlag = true;
   ProcessEvents<DAQEventReader, IPCPartition>(partition, events);
  }
  else {
   std::cout << " From file monitoring" << std::endl;
   g_onlineFlag = false;
   ProcessEvents<FileEventReader, string>(string(datafilename), events);
  }

  plotHistos();

//  myapp->Run(kTRUE);   // segfaults after exit ??
  myapp->Run();

  delete m_eventVectors;
  delete m_decoder;
  delete m_config;

  std::cout << " eventDisplay, going to exit" << std::endl;

  return 0;
}

template <typename EventReader, typename Source>
void
ProcessEvents(Source src, int events) {

  vector<unsigned int> rawEvent;
  EventReader evReader(src);

  g_pedestalSubtraction = !(string(m_config-> pedestal_filename()).empty());
  cout << "ProcessEvents" << " g_pedestalSubtraction = " << g_pedestalSubtraction << endl;

  if (g_pedestalSubtraction) {
    g_pedestals_stdev = (std::map<int, double>*)m_config->pedestal_stdev_map();
    std::cout << " Stdev s " <<  " size = " << g_pedestals_stdev->size() << std::endl;
//    for(auto it = g_pedestals_stdev->begin(); it != g_pedestals_stdev->end(); ++it ) {
//      std::cout << it->first << " " << it->second << "\n";
//    }
  }

  cout << "ProcessEvents" << "Number of events: " << events << endl;
  int eventCount = 0;
  while (eventCount < events || events == -1) {		// event loop
    cout << "\n # Event No.: " << eventCount << " events argument = " << events << endl;
    rawEvent = evReader.GetNextRawEvent();

    for(const unsigned int& it : rawEvent) {
//      cout << boost::format("%08x") % it << " "; 
    }

// specific code for ONE event monitoring programs
  if ((g_onlineFlag == true) && (eventCount == 1)) break;
  if ((g_onlineFlag == false) && (eventCount < (events-1))) {   // start at event 'eventCount'
    ++eventCount;
    continue;
  }

  int retBuild = buildEventVectors(rawEvent);

  unsigned int apv_evt =                        m_eventVectors->get_apv_evt();
  std::vector <unsigned int> apv_fec =  	m_eventVectors->get_apv_fec();
  std::vector <unsigned int> apv_id  =  	m_eventVectors->get_apv_id();
  std::vector <unsigned int> apv_ch  =  	m_eventVectors->get_apv_ch();
  std::vector <std::string>  mm_id = 		m_eventVectors->get_mm_id();
  std::vector <unsigned int> mm_readout =	m_eventVectors->get_mm_readout();
  std::vector <unsigned int> mm_strip	 =	m_eventVectors->get_mm_strip();
  std::vector <std::vector <short> > apv_q =	m_eventVectors->get_apv_q();
  std::vector <short> apv_qmax =                m_eventVectors->get_apv_qmax();
  std::vector <short> apv_tbqmax =              m_eventVectors->get_apv_tbqmax();
 
  std::cout << " apv_evt = " << apv_evt << std::endl;

  std::cout << " size of apv_fec = " << apv_fec.size() 
            << " fecNo[0] = " << apv_fec[0]  << std::endl;

  std::cout << " size of apv_id = " << apv_id.size()
            << " id[0] = " << apv_id[0]
            << " id[1] = " << apv_id[1]
            << " id[127] = " << apv_id[127]
            << " id[129] = " << apv_id[129]  << std::endl;

  std::cout << " size of apv_ch = " << apv_ch.size()
            << " ch[0] = " << apv_ch[0]  << std::endl;

  std::cout << " size of mm_id = " << mm_id.size()
            << " mm_id[0] = " << mm_id[0]
            << " mm_id[257] = " << mm_id[257]  << std::endl;

  std::cout << " size of mm_readout = " << mm_readout.size()
            << " mm_readout[0] = " << mm_readout[0]
            << " mm_readout[257] = " << mm_readout[257]  << std::endl;

  std::cout << " size of mm_strip = " << mm_strip.size()
              << " mm_strip[0] = " << mm_strip[0]
              << " mm_strip[257] = " << mm_strip[257]  << std::endl;

  int apvCh = 0;
  int apvChannelFirst = 0;
  int timeBin = 0;
  int timeBinMax = 0;
  bool firstTime = true;
  string currentChamber = mm_id[0];
  m_chamberNames.push_back(currentChamber);

  map<string,int> minStripChamber;
  map<string,int> maxStripChamber;
  map<string,float> minChargeChamber;
  map<string,float> maxChargeChamber;

  minChargeChamber[currentChamber] = 3000.0;
  maxChargeChamber[currentChamber] = -100.0;

  timeBinMax = apv_q[1].size();
  std::cout << " timeBinMax = " << timeBinMax << std::endl;
  std::cout << " currentChamber = " << currentChamber << std::endl;

  for (auto row = apv_q.begin(); row != apv_q.end(); ++row)
  {
    if (g_pedestalSubtraction) {
      std::vector<int16_t>::const_iterator imax = max_element(row->begin(), row->end());
//    std::cout << " ProcessEvents " << " apvCh = " << apvCh << " Q max = " << *imax << std::endl;
      int apvChNo = apvCh&0x7f;  //  channel number from 0 to 127 !!
      int apvChId = ((((apv_fec[apvCh]<<4) | apv_id[apvCh]) << 8) | apvChNo);	// copy makeChannelId
//    std::cout << " ProcessEvents " << " apvChId = " << apvChId << std::endl;
      double pedStd = (*g_pedestals_stdev)[apvChId];
//      std::cout << " ProcessEvents " << " pedStd = " << pedStd << std::endl;

    }

// one apvChannel (27 timebins)
       for (auto col = row->begin(); col != row->end(); ++col)
       {
//         std::cout << apvChannel << " " << timeBin << " " << *col << "  " ;
//         std::cout << "index = " << apvCh*timeBinMax + timeBin << " ";
//         std::cout << "mm_id = " << mm_id[apvCh]  <<  " ";

           if (firstTime) {  // points in graphs: 0,1,2.....,
             apvChannelFirst = apvCh;
             firstTime = false;
           }
           if (mm_id[apvCh] != currentChamber) {
             currentChamber = mm_id[apvCh];
             m_chamberNames.push_back(currentChamber);
             cout << " now chamber " << currentChamber << endl;
             minChargeChamber[currentChamber] = 3000.0;
             maxChargeChamber[currentChamber] = -100.0;
           }
           int stripNo = mm_strip[apvCh];
//         std::cout << "apvChannel = " << apvCh << " ";
//         std::cout << "index = " << (apvCh - apvChannelFirst)*timeBinMax + timeBin << " ";
//         std::cout << stripNo << " " << timeBin << " " << *col << "  " ;
           if (*col > 3200 ) {
              std::cout << stripNo << " " << timeBin << " " << *col << "  " ;
           }

           if (!g_pedestalSubtraction) {
             if (*col < minChargeChamber[currentChamber]) {
               minChargeChamber[currentChamber] = *col;
               minStripChamber[currentChamber] = mm_strip[apvCh];
               cout << " minChargeChamber " << " chamber = " << mm_id[apvCh] <<
                       " minCharge = " <<  minChargeChamber[currentChamber] <<
                       " minStrip = " <<  minStripChamber[currentChamber] << endl;
             }
           }
           else { // pedestal subtraction
             if (*col > maxChargeChamber[currentChamber]) {
               maxChargeChamber[currentChamber] = *col;
               maxStripChamber[currentChamber] = mm_strip[apvCh];
               cout << " maxChargeChamber " << " chamber = " << mm_id[apvCh] <<
                       " maxCharge = " <<  maxChargeChamber[currentChamber] <<
                       " maxStrip = " <<  maxStripChamber[currentChamber] <<  endl;
             }
           }

           timeBin++;
       }
//       timeBinMax = timeBin;
       timeBin = 0;
       apvCh++;
   }

   getGeometry();

   g_track = new TGraph();
   g_track->SetTitle("Track;Z(mm);X(mm)");
   gStyle->SetOptFit(0111); // print fit parameters

// RD51 2017/12/07
// the origin of the coordinate system is the first strip of the chambers
// supposed to be aligned !!
// strips are counting along the negative x-axis so away from the CR TO BE CHECKED
// small strip counts correspond to positive x cooordinates
   float xRel;
   float midStrip;
   for (int ii = 0; ii<m_chamberNames.size(); ii++) {
//     midStrip = (m_maxStrips[m_chamberNames[ii]]+1.0)/2.0;  // mid strip = 512.5
     midStrip = 1.0;  // to be reviewed
     xRel = (midStrip + float(maxStripChamber[m_chamberNames[ii]])) * m_pitch[m_chamberNames[ii]];
     std::cout << " z = " << m_zPos[m_chamberNames[ii]] << " xRel = " << xRel << std::endl;
     g_track->SetPoint(ii,m_zPos[m_chamberNames[ii]],xRel);
   }
   TFitResultPtr r = g_track->Fit("pol1","S");
   cout << " slope = " << r->Value(0) << " intercept = " << r->Value(1) << endl;
//   r->Print("V");     // print full information of fit including covariance matrix

   double theta = atan(r->Value(1)) * 180 / PI;
   cout << " theta = " << theta << " degrees " << endl;

   plotHistos();

   ++eventCount;
   cout << "\n" << endl;
 }
}

void getGeometry(void) {

// homebuilt way of getting geometry of chambers; see recomm
   float maxStrips;

   TEnv* tenv = (TEnv*)m_config->get_tenv_config();
   for (int ii = 0; ii<m_chamberNames.size(); ii++) {
     stringstream ss;
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".ZPos";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     string str1 = tenv->GetValue(ss.str().c_str(), "");
//     cout << " str1= " << str1 << endl;
     m_zPos[m_chamberNames[ii]] = stof(str1);
     cout << "  chamber name = " << m_chamberNames[ii] << " zPos = "  << m_zPos[m_chamberNames[ii]] << endl;
     m_deltaZpos = m_zPos[m_chamberNames[ii]] - m_zPos[m_chamberNames[0]];

     ss.str("");
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".Strips.X.Pitch";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     str1 = tenv->GetValue(ss.str().c_str(), "");
     m_pitch[m_chamberNames[ii]] = stof(str1);
     cout << "  chamber name = " << m_chamberNames[ii] << " pitch = "  << m_pitch[m_chamberNames[ii]] << endl;

     ss.str("");
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".Strips.X.Max";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     str1 = tenv->GetValue(ss.str().c_str(), "");
     maxStrips = stof(str1);
     cout << "  chamber name = " << m_chamberNames[ii] << " max strip = "  << maxStrips << endl;
     m_maxStrips[m_chamberNames[ii]] = maxStrips; // 1024
   }
}

void init_mm( int argcConfig, char* pArg []) {

// construct MM objects: config, decoder, writer and event vectors
  try {
    m_config = new CConfiguration(argcConfig, pArg, "dummy");
  }
  catch (std::string str) {
     throw runtime_error(string("Errors in Configuration: ") + str);
  }
  if (m_config->error()) {
     throw runtime_error("Other Errors in configuration: ");
  }

  m_save_data_flag = true;             // REVIEW

  m_config->run_type(m_preset_run_type);

  m_decoder = new CEventDecoder(m_config, &m_datacontainer, &m_eventcontainer);
//  m_decoder->attach(m_receiverFile);

  m_decoder->preset_event_type(m_preset_event_type);

  m_eventVectors = new CEventVectors(m_config, &m_eventcontainer, m_save_data_flag);

  std::cout << " eventDisplay, CEventVectors OK " << std::endl;

}

void 
term_handler(int param) {
  assert(param == 15);
  std::cout << "Handling SIGTERM... exiting" << std::endl;
  exit(0);
}

int buildEventVectors(std::vector<unsigned int>& rawEvent) {

  int err = 0;

  buildFrameContainer(rawEvent,&m_datacontainer );

  err = m_decoder->buildEventContainer();
  std::cout << " ProcessEvents" << " after buildEventContainer, err = " << err << std::endl;

  std::cout << " ProcessEvents " << " size of event container = " << m_eventcontainer.size() << std::endl;

  err = m_eventVectors->buildVectors();

  std::cout << " ProcessEvents" << " after buildVectors(), err = " << err << std::endl;

  return err;

}

void plotHistos () {

  g_track->Draw("AL*");
// Graph mysteries
  g_track->GetXaxis()->SetLimits(m_zPos[m_chamberNames[0]]-10.0,m_deltaZpos+10.0);
  g_track->GetYaxis()->SetRangeUser(-250.0,250.0);  // +- 25 cm
  g_track->Draw("AL*");
  g_canvas0->Update();

}
