/*
 * adapted to BL4S Jan,17 J.Petersen
 */

#ifndef microMegas_h
#define microMegas_h

#include <list>
#include <string>
#include <vector>

class CConfiguration;
class CLogger;
class CReceiver;
class CReceiverFile;
class CEventDecoder;
class CMMEvent;
class CUDPData;
class CRootWriter;
class CPublisher;

   CConfiguration*	m_config;
   CLogger* 		m_logger;
   CReceiver*           m_receiver;
   CReceiverFile*       m_receiverFile;
   CEventDecoder*       m_decoder;
   CRootWriter*         m_writer;
   CPublisher*          m_publisher;

   std::list <CUDPData*>        m_datacontainer;
   std::list <CMMEvent*>        m_eventcontainer;

   CEvent::event_type_type      m_preset_event_type;
   CConfiguration::runtype_t    m_preset_run_type;

   std::string                  m_commandLine;

   time_t               m_run_start_time;
   std::vector <long>   m_internal_queue_sizes;
   bool m_save_data_flag;
   bool m_busy;
   size_t m_received_events_counter;

#endif
