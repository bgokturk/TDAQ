//$Id: DataChannelV560.h 132337 2015-04-02 07:55:30Z joos $
/********************************************************/
/*							*/
/* Date: 17 November 2006				*/ 
/* Author: Markus Joos					*/
/*							*/
/*** C 2015 - The software with that certain something **/

#ifndef DATACHANNELV560_H
#define DATACHANNELV560_H

#include "ROSCore/SingleFragmentDataChannel.h"

namespace ROS 
{
  class DataChannelV560 : public SingleFragmentDataChannel 
  {
  public:    
    DataChannelV560(u_int id, 
	            u_int configId, 
                    u_int rolPhysicalAddress, 
                    u_long vmeBaseVA,
                    u_int numberOfScalerChannels,  
                    DFCountedPointer<Config> configuration);    
    virtual ~DataChannelV560() ;
    virtual int getNextFragment(u_int* buffer, int max_size, u_int* status, u_long pciAddress = 0);
    virtual DFCountedPointer < Config > getInfo();

  private:
    v560_regs_t *m_v560;
    u_int m_channelId;
    u_int m_rolPhysicalAddress;
    u_int m_numberOfScalerChannels;
    
    enum Statuswords 
    {
      S_OK = 0,
      S_OVERFLOW
    };
  };
}
#endif //DATACHANNELV560_H
