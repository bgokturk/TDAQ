#ifndef V560EXCEPTION_H
#define V560EXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>
#include <iostream>

namespace RCD 
{
  using namespace ROS;

  class V560Exception : public ROSException 
  {
  public:
    enum ErrorCode 
    {
      TOOMANYCHANNELS = 1,
      ILLEGALROLPHYSICALADDRESS
    };
    V560Exception(ErrorCode errorCode);
    V560Exception(ErrorCode errorCode, std::string description);
    V560Exception(ErrorCode errorCode, const ers::Context& context);
    V560Exception(ErrorCode errorCode, std::string description, const ers::Context& context);
    V560Exception(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

    virtual ~V560Exception() throw() { }

  protected:
    virtual std::string getErrorString(u_int errorId) const;
  };

  inline std::string V560Exception::getErrorString(u_int errorId) const 
  {
    std::string rc;
    switch (errorId) 
    {
    case TOOMANYCHANNELS:
      rc = "More than one data channel";
      break;
    case ILLEGALROLPHYSICALADDRESS:
      rc = "ROL physical address not equal to 0 or 1";
      break;
    default:
      rc = "";
      break;
    }
    return rc;
  }
  
}
#endif //V560EXCEPTION_H
