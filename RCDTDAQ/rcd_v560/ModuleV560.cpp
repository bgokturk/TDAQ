//$Id: ModuleV560.cpp 132336 2015-04-02 07:55:26Z joos $
/********************************************************/
/*							*/
/* Date: 17 November 2006				*/ 
/* Author: Markus Joos, J.O.Petersen			*/
/*							*/
/*** C 2015 - The software with that certain something **/

#include <unistd.h>
#include <iostream>
#include <fcntl.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModules/ModulesException.h"
#include "rcd_v560/v560.h"
#include "rcd_v560/DataChannelV560.h"
#include "rcd_v560/ModuleV560.h"
#include "rcd_v560/ExceptionV560.h"


using namespace ROS;
using namespace RCD;


/**********************/
ModuleV560::ModuleV560() 
/**********************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::constructor: Entered");
}


/*********************************/
ModuleV560::~ModuleV560()  noexcept
/*********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::destructor: Entered");

  while (m_dataChannels.size() > 0) 
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;		// we created them
  }
}


/************************************************************/
void ModuleV560::setup(DFCountedPointer<Config> configuration) 
/************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::setup: Entered");

  m_configuration = configuration;

  m_numberOfDataChannels = configuration->getInt("NumberOfChannels");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV560::setup: numberOfDataChannels = " << m_numberOfDataChannels);

  if (m_numberOfDataChannels > 1) 
    throw V560Exception(V560Exception::TOOMANYCHANNELS); 

  // channel parameters
  for(int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    m_id.push_back(0);
    m_rolPhysicalAddress.push_back(0);
  }

  //get VMEbus parameters for the V560 MODULE
  m_vmeAddress = configuration->getInt("VMEbusAddress");
  m_vmeWindowSize = configuration->getInt("VMEbusWindowSize");
  m_vmeAddressModifier = configuration->getInt("VMEbusAddressModifier");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV560::setup: vmeAddress = " << HEX(m_vmeAddress) << "  vmeWindowSize  = " << HEX(m_vmeWindowSize) << "  vmeAddressModifier = " << HEX(m_vmeAddressModifier));

  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    m_id[chan] = configuration->getInt("channelId", chan);
    m_rolPhysicalAddress[chan] = configuration->getInt("ROLPhysicalAddress", chan);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV560::setup: chan = " << chan << "  id = " << m_id[chan] << " rolPhysicalAddress = " << m_rolPhysicalAddress[chan]);
    if (m_rolPhysicalAddress[chan] > 1) 
      throw V560Exception(V560Exception::ILLEGALROLPHYSICALADDRESS); 
  }

  m_numberOfScalerChannels = configuration->getInt("NumberOfScalerChannels");

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::setup: Done");
}


/*******************************************************/
void ModuleV560::configure(const daq::rc::TransitionCmd&)
/*******************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::configure: Entered");

  err_type ret;
  err_str rcc_err_str;

  ret = VME_Open();
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "VMEReadoutModuleInterrupt::configure: Error from VME_Open");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, ModulesException, VME_OPEN, rcc_err_str);
    throw (ex1);
  }

  //Create the DataChannel object
  VME_MasterMap_t master_map;
  master_map.vmebus_address   = m_vmeAddress;
  master_map.window_size      = m_vmeWindowSize;
  master_map.address_modifier = m_vmeAddressModifier;
  master_map.options          = 0;

  ret = VME_MasterMap(&master_map, &m_vmeScHandle);
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleV560::configure: Error from VME_MasterMap");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex2, ModulesException, VME_MASTERMAP, rcc_err_str);
    throw (ex2);
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV560::configure: VME_MasterMap, handle = " << m_vmeScHandle);

  ret = VME_MasterMapVirtualLongAddress(m_vmeScHandle, &m_vmeVirtualPtr);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV560::configure: VME_MasterMapVirtualAddress, VA = " << HEX(m_vmeVirtualPtr));
  m_v560 = reinterpret_cast<v560_regs_t*>(m_vmeVirtualPtr);  // (virtual) base address of the channel

  for (int chan = 0; chan < m_numberOfDataChannels; chan++) 
  {
    // chan == the Config Id
    DataChannelV560 *channel = new DataChannelV560 (m_id[chan],
                                                    chan,
                                                    m_rolPhysicalAddress[chan],
                                                    m_vmeVirtualPtr,
						    m_numberOfScalerChannels,
                                                    m_configuration);
    m_dataChannels.push_back(channel);
  }

  //Initialize the V560 card
  m_v560->scale_clear = 0;
  m_v560->vme_veto_set = 0;  //Block the counter

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::configure: Done");
}


/*********************************************************/
void ModuleV560::unconfigure(const daq::rc::TransitionCmd&)
/*********************************************************/
{
  err_type ret;
  err_str rcc_err_str;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::unconfigure: Entered");

  ret = VME_MasterUnmap(m_vmeScHandle);
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleV560::unconfigure: Error from VME_MasterUnmap");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex3, ModulesException, VME_MASTERUNMAP, rcc_err_str);
    throw (ex3);
  }

  ret = VME_Close();
  if (ret != VME_SUCCESS) 
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleV560::unconfigure: Error from VME_Close");
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex4, ModulesException, VME_CLOSE, rcc_err_str);
    throw (ex4);
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::unconfigure: Done");
}


/*****************************************************/
void ModuleV560::connect(const daq::rc::TransitionCmd&)
/*****************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "ModuleV560::connect: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::connect: Done");
}


/********************************************************/
void ModuleV560::disconnect(const daq::rc::TransitionCmd&)
/********************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::disconnect: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::disconnect: Done");
}
    

/***********************************************************/    
void ModuleV560::prepareForRun(const daq::rc::TransitionCmd&)
/***********************************************************/    
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::prepareForRun: Entered");

  m_v560->scale_clear = 0;     //Clear the counters
  m_v560->vme_veto_reset = 0;  //Unblock the counters

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::prepareForRun: Done");
}


/****************************************************/
void ModuleV560::stopDC(const daq::rc::TransitionCmd&)
/****************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::stopDC: Entered");
  //m_v560->vme_veto_set = 0;  //Block the counter
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::stopDC: Done");
}


/***********************************************/
DFCountedPointer < Config > ModuleV560::getInfo()
/***********************************************/
{
  u_short v1, v2, v3;
  
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::getInfo: Entered");
  DFCountedPointer<Config> info = Config::New();
   
  v1 = m_v560->fixed_code; 
  v2 = m_v560->manufacturer_module_type; 
  v3 = m_v560->version_series;

  info->set("V560 Board serial number", v3 & 0xfff);
  info->set("V560 Board version", (v3 >> 12) & 0xf);
  info->set("V560 Board type", v2 & 0x3ff);
  info->set("V560 Board manufacturer", (v2 >> 10) & 0x3f);
  info->set("V560 CAEN fixed code", v1);
  
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::getInfo: Done");
  return(info);
}


/**************************/
void ModuleV560::clearInfo()
/**************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::clearInfo: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleV560::clearInfo: Done");
}


//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createReadoutModuleV560();
}
ReadoutModule* createReadoutModuleV560()
{
  return (new ModuleV560());
}





