#include <exception>

#include <TH1.h>
#include <TFile.h>

#include <oh/OHRootProvider.h>

#include "MonitorScaler.h"
#include "BL4SConfig.h"
#include "CommonLibrary/RCDRawEvent.h"

using namespace std;

MonitorScaler::MonitorScaler(std::string name, BL4SConfig * cfg, 
                             std::string runNumberStr, bool publish )
{
  m_MonitorName = name;
  m_scaler_channel_map = cfg->GetScalerChannels();

  // BEFORE booking histograms
  if (!publish) {
    PrepareRootFile(runNumberStr);
  }

  int Nscalers = m_scaler_channel_map.size();
  std::cout << " MonitorScaler::MonitorScaler " << " # scalers channels = " << Nscalers << std::endl;

  for (map<string,int>::iterator it=m_scaler_channel_map.begin(); it!=m_scaler_channel_map.end(); ++it) {
    string scaler_channel_name = it->first;
//      std::cout << " channel number = " << it->second << " channel name = " << scaler_channel_name << std::endl;
    TGraph* curGraph = new TGraph();
    string title = " scaler versus time;";
    string titleX = "time[ms];";
    string titleY = scaler_channel_name;
    string graphTitle = title + titleX + titleY;
    curGraph->SetTitle(graphTitle.c_str());
    m_scVsTime.push_back(curGraph);
  }
  m_graphCount = 0;

// get channel number of Timer  DON'T change the name!!
  std::map<string,int>::iterator it;
// should check if found ..
  it = m_scaler_channel_map.find("Timer");
  if (it == m_scaler_channel_map.end()){
    throw runtime_error("Timer: channel not found in config");
  }
  m_channelNoTimer = it->second;
  std::cout <<" MonitorScaler::MonitorScaler " << " Timer channel No = " << m_channelNoTimer << std::endl;

// snapshot of scaler
  m_scaler_counts = new TH1I("scaler_counts",
                                 "scaler counts;Name;# of counts" ,Nscalers,0,Nscalers);
  // give the bins names
  int ii = 0;
  for (map<string,int>::iterator it=m_scaler_channel_map.begin(); it!=m_scaler_channel_map.end(); ++it) {
    m_scaler_counts->GetXaxis()->SetBinLabel(ii+1,it->first.c_str());
    ii++;
  }
  m_scaler_counts->SetMinimum(0);
}

MonitorScaler::~MonitorScaler()
{
}

bool
MonitorScaler::Process(RCDRawEvent& ev)
{
 if (ev.v560_data.size() == 0 ){
   cerr << "MonitorScaler::Process No scaler data found in event " << ev.getEventNumber() << endl;
   return false;
 }
 int ii=0;
 for (map<string,int>::iterator it=m_scaler_channel_map.begin(); it!=m_scaler_channel_map.end(); ++it) {
   int channelNo = it->second;
//   cout << std::endl << "MonitorScaler::Process" << " timer = " << ev.v560_data[0].Channel[m_channelNoTimer].Data
//        << " channel = " << ev.v560_data[0].Channel[channelNo].Data << endl;

   m_scVsTime[ii]->SetPoint(m_graphCount,
                          (double)ev.v560_data[0].Channel[m_channelNoTimer].Data,
                          (double)ev.v560_data[0].Channel[channelNo].Data);
   ii++;
 }
 m_graphCount++;

// snapshot of scalers
  int isc = 0;
  for (map<string,int>::iterator it=m_scaler_channel_map.begin(); it!=m_scaler_channel_map.end(); ++it) {
    int channelNo = it->second;
    string name = it->first;
    int count = ev.v560_data[0].Channel[channelNo].Data;
    m_scaler_counts->SetBinContent(isc+1,(double)count);
    isc++;
  }

 return true;
}

void
MonitorScaler::Print(RCDRawEvent& ev)
{
  cout << std::dec << "Scaler Modules: " << ev.v560_data.size() << "\n";
// over modules
    for(vector<V560Data>::iterator v560 = ev.v560_data.begin(); v560 !=  ev.v560_data.end(); v560++) {
      int ch = 0;
      for (int i=0;i<16;i++) {  // HW to 16 as in VMEStructure
        cout << ch << ": " << hex << v560->Channel[ch].Data
             << " " <<   dec << v560->Channel[ch].Data << ", ";
        ++ch;
      }
    cout << endl;
  }

}

void
MonitorScaler::WriteToFile() {

// graphs are not in TFile
  int ii=0;
  for (map<string,int>::iterator it=m_scaler_channel_map.begin(); it!=m_scaler_channel_map.end(); ++it) {
    string scaler_channel_name = it->first;
    m_scVsTime[ii]->Write(scaler_channel_name.c_str());
    //std::cout << " ii = " << ii << std::endl;
    std::cout << " MonitorScaler::WriteToFile(), scaler_channel_name = " << scaler_channel_name << std::endl;

    ii++;
  }

  m_outputFile->Write();
  m_outputFile->Close();
}

void
MonitorScaler::PublishHists() {

   m_histProvider->publish(*m_scaler_counts, "Counts");

   int iGraph=0;

   for (auto it=m_scaler_channel_map.begin(); it!=m_scaler_channel_map.end(); ++it) {

     int channelNo = it->second;
     TGraph *currGraph = m_scVsTime[iGraph];

     string channelName = it->first;
     int count = currGraph->GetY()[currGraph->GetN()-1];

/*****
     cout << "Channel " 
          << std::setw(2) << std::setfill('0') << channelNo 
          << std::setfill(' ')
          << " - " << std::left << std::setw(15) << channelName << ": "
          << std::right << std::setw(8) << count << endl;a
*****/

     std::ostringstream graphNameStream;
     graphNameStream << "CountsVsTime_Ch"
                     << std::setw(2) << std::setfill('0') << channelNo
                     << "_" << channelName;

     m_histProvider->publish(*currGraph, graphNameStream.str());
     ++iGraph;
   }

//   cout << std::endl << "MonitorScaler::Process" << " timer = " << ev.v560_data[0].Channel[m_channelNoTimer].Data
//        << " channel = " << ev.v560_data[0].Channel[channelNo].Data << endl;
}

void MonitorScaler::PrepareRootFile(std::string runNumberStr) {

// Store histograms in root file mirroring the OH hierarchy
// construct the ROOT file name here in 'user' space
// BEFORE booking histograms

  TDirectory * histogramming_dir = 0;
  TDirectory * monitor_dir = 0;

  const char * histogramming = "Histogramming";
  const char * monitor = "Scaler_monitor";
  string output_name;

  output_name.append("/afs/cern.ch/work/d/daquser/public/bl4smonitor/");
  output_name.append(runNumberStr);
  output_name.append(monitor);
  output_name.append(".root");
  printf(" MonitorChamber::prepareRootFile, Output file name: %s\n", output_name.c_str());
  m_outputFile = new TFile(output_name.c_str(), "RECREATE");
// give Tfile a structure
  histogramming_dir = m_outputFile->mkdir(histogramming);
  histogramming_dir->cd();
  monitor_dir = histogramming_dir->mkdir(monitor);
  monitor_dir->cd();    // current directory

}

