#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>
 
#include "VMEStructure.h"
#include "eudaq.h"

#ifndef __RCD_RAWEVENT_H__
#define __RCD_RAWEVENT_H__
 
using namespace std;

class RCDRawEvent {
  public:
    RCDRawEvent(vector<u_int>  & vec_rawev);
    ~RCDRawEvent();

    bool isValid(){ return m_valid; }
    u_int getEventNumber(){ return m_event_num; }

    std::vector<V792Data> v792_data;    // QDC
    std::vector<V1290Data> v1290_data;  // TDC
    std::vector<V560Data> v560_data;    // Scaler
    std::vector<MimosaData> mimosa_data; // Mimosa 

    static uint GetEventNumber(vector<u_int> & vec_rawev);

  private:
    u_int event_size; //Size of the event in 32 bit words
    vector<u_int> vec_raw; // raw event data
    vector <u_int>::iterator m_iterator;
    bool m_valid;
    u_int m_event_num;

    void Readv792();
    void Readv1290();
    void Readv560();
    void ReadMimosa();

    // Helper functions for reading the mimosa data
    MimosaData readMimosaFrames();
    uint DecodeMimosaData(MimosaPlane &plane, vector <u_int>::iterator& iterator, uint32_t len);


    eudaqFlags m_eudaqFlags;
    bool m_eudaqFlagsSet;

    void ReadEventHeader();
    bool FindAndReadModule();
    //u_int FindModuleHeader();
    void ReadModuleData(u_int ModuleID);
    static bool IsModuleHeader(u_int word);
};
#endif
