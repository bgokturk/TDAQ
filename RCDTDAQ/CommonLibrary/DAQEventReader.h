#ifndef __DAQ_EVENT_READER_H__
#define __DAQ_EVENT_READER_H__

#include <emon/EventIterator.h>
#include <ipc/core.h>
#include <vector>

#include "EventReaderBase.h"


class DAQEventReader : public EventReaderBase {
  public:
    DAQEventReader(IPCPartition& partition, bool multiRun=false);
    ~DAQEventReader() {};

    virtual int GetRunNumber(){ return 0;}
    virtual int GetNumberOfEvents(){ return -1;}
    virtual std::vector<unsigned int> GetNextRawEvent();

  private:
    IPCPartition& fPartition;
    emon::Event fEvent;
    unsigned int fEventCount;
    unsigned fAsync;
    bool fMultiRun; //Not implemented yet
    std::unique_ptr<emon::EventIterator> fIt;
    u_int fBufferSize;

};

  
#endif
