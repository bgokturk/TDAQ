#ifndef __EVENT_LOOPER_H__
#define __EVENT_LOOPER_H__

#include "RCDRawEvent.h"
#include "DAQEventReader.h"
#include "FileEventReader.h"

#include <oh/OHRootProvider.h>
#include <TFile.h>
#include <TH1.h>

#include <time.h>
// #include <sstream>
#include <vector>

#include "MonitorRaw.h"
#include "MonitorPhysics.h"


class EventLooper {
  public:
    EventLooper();
    template <typename EventReader, typename Source>
    void ProcessEvents(Source src, int nEvents);

    void SetMonitoringCutsFile(std::string moncutsfilename) { fMoncutsfilename = moncutsfilename; }
    void SetDataOutFileName(std::string datafilename) { fDataFile = datafilename; }
    void SetOutFileDirectory(std::string outfiledirectory) { fOutfiledirectory = outfiledirectory; }
    void SetPublishCycleTime(long int cycleTime) { fPublishCycleTime = cycleTime; };
    void SetHistProvider(OHRootProvider& prov) 
    {
      fHistProvider = &prov;
      fPublishHists = true;
    } 

    bool IsTimeToPublish();

    template <typename Monitor>
    void PublishHists(Monitor& monitor);
    template <typename Monitor>
    void WriteToFile(Monitor& monitor);

  private:
    OHRootProvider* fHistProvider;
    bool fPublishHists;
    std::string fMoncutsfilename;
    std::string fDataFile;
    std::string fOutfiledirectory;
    bool fRootFileCreated;
    long int fPublishCycleTime;
    time_t fLastPublicationTime;
};


#endif
