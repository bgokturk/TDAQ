#include "HistogramStore.h"
//#include "xAODRootAccess/tools/Message.h"
#include "TH1.h"
#include "TH1F.h"
#include "TString.h"
#include <iostream>
#include <vector>

/// Helper macro for checking histogram existanse in histogram list
#define HIST_DECLARED( HISTTYPE, NAME, EXP )                                                                    \
  {                                                                                                             \
    if (EXP) {                                                                                                  \
      printf("\nFATAL\n HistogramStore::%s Attempt to create second histogram named %s \n\n", HISTTYPE, NAME);  \
      abort();                                                                                                  \
    }                                                                                                           \
  }

/// Helper macro for checking histogram access in histogram list
#define HIST_ACCESSED( HISTTYPE, NAME, EXP )                                                                    \
  {                                                                                                             \
    if (EXP) {                                                                                                  \
      printf("\nFATAL\n HistogramStore::%s requested %s cannot be accessed. \n\n", HISTTYPE, NAME);             \
      abort();                                                                                                  \
    }                                                                                                           \
  }

// Create and store TH1F histogram
void HistogramStore::createTH1F(TString name, int Nbins, double xmin, double xmax, TString title) 
{
  HIST_DECLARED("createHistoTH1F", name.Data(), hasTH1F(name));
  m_histoTH1F[name] = new TH1F(name,title,Nbins,xmin,xmax);
  // m_histoTH1F[name]->Sumw2();
}

// Create and Store TH1F histogram
void HistogramStore::createTH1F(TString name, const std::vector<double> &bins, TString title) 
{
  HIST_DECLARED("createHistoTH1F", name.Data(), hasTH1F(name));
  m_histoTH1F[name] = new TH1F(name,title,-1+bins.size(),&bins[0]);
  // m_histoTH1F[name]->Sumw2();
}

// Create and store TH2F histogram
void HistogramStore::createTH2F(TString name, int NbinsX, double xmin, double xmax, int NBinsY, double ymin, double ymax, TString title) 
{
  HIST_DECLARED("createHistoTH2F", name.Data(), hasTH2F(name));
  m_histoTH2F[name] = new TH2F(name, title, NbinsX, xmin, xmax, NBinsY, ymin, ymax);
  // m_histoTH2F[name]->Sumw2();
}

// Create and store TH2F histogram
void HistogramStore::createTH2F(TString name, const std::vector<double> &xbins, const std::vector<double> &ybins, TString title) 
{
  HIST_DECLARED("createHistoTH2F", name.Data(), hasTH2F(name));
  m_histoTH2F[name] = new TH2F(name, title, xbins.size() - 1, &xbins[0],  ybins.size() - 1, &ybins[0]);
  // m_histoTH2F[name]->Sumw2();
}

// Create and store TH2F histogram
void HistogramStore::createTH2F(TString name, const std::vector<double> &xbins, int NBinsY, double ymin, double ymax, TString title) 
{
  HIST_DECLARED("createHistoTH2F", name.Data(), hasTH2F(name));
  m_histoTH2F[name] = new TH2F(name, title, xbins.size() - 1, &xbins[0],  NBinsY, ymin, ymax);
  // m_histoTH2F[name]->Sumw2();
}

// Create and store TH3F histogram
void HistogramStore::createTH3F(TString name, int NbinsX, double xmin, double xmax, int NBinsY, double ymin, double ymax, 
                      int NbinsZ, double zmin, double zmax, TString title) 
{
  HIST_DECLARED("createHistoTH3F", name.Data(), hasTH3F(name));
  m_histoTH3F[name] = new TH3F(name, title, NbinsX, xmin, xmax, NBinsY, ymin, ymax, NbinsZ, zmin, zmax);
  // m_histoTH3F[name]->Sumw2();
}

// Create and store TH3F histogram
void HistogramStore::createTH3F(TString name, const std::vector<double> &xbins, const std::vector<double> &ybins, 
                    const std::vector<double> &zbins, TString title) 
{
  HIST_DECLARED("createHistoTH3F", name.Data(), hasTH3F(name));
  m_histoTH3F[name] = new TH3F(name, title, xbins.size() - 1, &xbins[0],  ybins.size() - 1, &ybins[0], zbins.size() - 1, &zbins[0]);
  // m_histoTH3F[name]->Sumw2();
}

// Create and store TProfile histogram
void HistogramStore::createTProfile(TString name, int NbinsX, double xmin, double xmax, TString title) 
{
  HIST_DECLARED("createHistoTProfile", name.Data(), hasTH1F(name));
  m_histoTProfile[name] = new TProfile(name, title, NbinsX, xmin, xmax);
  // m_histoTProfile[name]->Sumw2();
}

// Create and store TProfile histogram
void HistogramStore::createTProfile(TString name, const std::vector<double> &xbins, TString title) 
{
  HIST_DECLARED("createHistoTProfile", name.Data(), hasTProfile(name));
  m_histoTProfile[name] = new TProfile(name, title, xbins.size() - 1, &xbins[0]);
  // m_histoTProfile[name]->Sumw2();
}

// Create and store TProfile histogram
void HistogramStore::createTProfile(TString name, int nbins, const double xlow, const double xup, const double ylow, const double yup, TString title) 
{
  HIST_DECLARED("createHistoTProfile", name.Data(), hasTProfile(name));
  m_histoTProfile[name] = new TProfile(name, title, nbins, xlow, xup, ylow, yup);
  // m_histoTProfile[name]->Sumw2();
}


// Retrieve a TH1F histogram from the internal store
TH1F* HistogramStore::getTH1F(TString name)
{
  HIST_ACCESSED("getTH1F", name.Data(), !hasTH1F(name));
  return m_histoTH1F[name];
}

// Retrieve a TH2F histogram from the internal store
TH2F* HistogramStore::getTH2F(TString name)
{
  HIST_ACCESSED("getTH2F", name.Data(), !hasTH2F(name));
  return m_histoTH2F[name];
}

// Retrieve a TH3F histogram from the internal store
TH3F* HistogramStore::getTH3F(TString name)
{
  HIST_ACCESSED("getTH3F", name.Data(), !hasTH3F(name));
  return m_histoTH3F[name];
}

// Retrieve a TH2F histogram from the internal store
TProfile* HistogramStore::getTProfile(TString name)
{
  HIST_ACCESSED("getTProfile", name.Data(), !hasTProfile(name));
  return m_histoTProfile[name];
}



// Return vector of all histograms in internal store
std::vector<TH1*> HistogramStore::getListOfHistograms()
{
  std::vector<TH1*> allHistos;
  // an iterator of a map is pair<keyType,valueType>
  for (auto h : m_histoTH1F ) allHistos.push_back(h.second);
  for (auto h : m_histoTH2F ) allHistos.push_back(h.second);
  for (auto h : m_histoTH3F ) allHistos.push_back(h.second);
  for (auto h : m_histoTProfile ) allHistos.push_back(h.second);
  return allHistos;
}
