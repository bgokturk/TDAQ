#ifndef __MONITORING_CUTS__ 
#define __MONITORING_CUTS__ 

#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <iomanip>

class MonitoringCuts {
	private:
    std::string fMoncutsfilePass; 
    std::map<std::string, std::pair<double,double>> fCutValues;

  public:
    MonitoringCuts();
    MonitoringCuts(std::string moncutsfilename);
    ~MonitoringCuts();

    void SetCutsFile(std::string moncutsfilename);
    void SetMonitoringCuts(std::string moncutsfilename);
    void ParseFile();

    void ShowAllCuts();
    
    double getMinCut(std::string name);
  	double getMaxCut(std::string name);

  private:
  	int ReadLine( std::string inpline );

  	void createCut(std::string name, double minCut, double maxCut);
  	inline bool hasCutName(std::string name) {return fCutValues.count(name)>0;}
};

  
#endif
