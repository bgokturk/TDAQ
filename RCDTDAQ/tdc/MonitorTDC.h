#ifndef __MONITOR_TDC__ 
#define __MONITOR_TDC__ 

#include <vector>

#include "CommonLibrary/MonitorBase.h"

class BL4SConfig;
class OHRootProvider;
class TH1I;
class TFile;

class MonitorTdc: public MonitorBase {
  public:
    MonitorTdc();
    MonitorTdc(std::string name, BL4SConfig* cfg, std::string runNumber, bool publish);
    ~MonitorTdc();

    virtual bool Process(RCDRawEvent& ev);
    virtual void Print(RCDRawEvent& ev);
    virtual void WriteToFile();
    virtual void PublishHists();
    virtual void PrepareRootFile(std::string runNumberStr);

  private:
    TFile * m_outputFile;
    std::string m_MonitorName;

    std::vector<TH1I*> m_TDC_time;
    std::vector<TH1I*> m_TDC_Ncounts;

};

  
#endif
