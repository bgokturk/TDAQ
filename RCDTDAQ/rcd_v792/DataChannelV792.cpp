
/********************************************************/
/*							*/
/* Date: 31 October 2006				*/ 
/* Author: Markus Joos					*/
/*							*/
/*** C 2006 - The software with that certain something **/

#include <stdint.h>
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"
#include "rcd_v792/ExceptionV792.h"
#include "rcd_v792/v792.h"
#include "rcd_v792/DataChannelV792.h"

using namespace ROS;
using namespace RCD;


/**********************************************************************************************/
DataChannelV792::DataChannelV792(u_int channelId,
				 u_int channelIndex,
				 u_int rolPhysicalAddress,
				 u_long vmeBaseVA,
				 DFCountedPointer<Config> configuration,
				 DataChannelV792Info *info) :
  SingleFragmentDataChannel(channelId, channelIndex, rolPhysicalAddress, configuration, info)
/**********************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV792::constructor: Entered");
  m_v792 = reinterpret_cast<v792_regs_t*>(vmeBaseVA);  // (virtual) base address of the channel
  m_statistics = info;
  m_channel_number = rolPhysicalAddress;
  m_channelId = channelId;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelV792::constructor: rolPhysicalAddress = " << m_channel_number);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV792::constructor: Done");
}


/*********************************/
DataChannelV792::~DataChannelV792() 
/*********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV792::destructor: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV792::destructor: Done");
}


/*********************************************************************************************************/
int DataChannelV792::getNextFragment(u_int* buffer, int max_size, u_int* status, unsigned long pciAddress)
/*********************************************************************************************************/
{
  u_int toomuch, loop, num, fsize = 0, data, timeout = 0;
  u_short value;
    
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV792::getNextFragment: Entered for channel = " << m_channel_number);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV792::getNextFragment: max_size = " << max_size);

  u_int* bufPtr = buffer;        // the buffer address


  //Write the channel number
  *bufPtr++ = m_channelId;
  fsize += 4;

  *bufPtr++ = CAEN_QDC_v792x; // Write the module type
  fsize += 4;

  //Wait until there is an event ready
  value = m_v792->status_register_1;
  while (!(value & 0x1))
  {
    timeout++;     
    if (timeout == 1000000)   //with timeout = 1000000 we will wait for about 1 s
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV792::getNextFragment: ERROR: Time out when waiting for DREADY on channel " << m_channel_number);

      //prepare an empty event
      *bufPtr = 0;
      fsize = 8;
      *status = S_TIMEOUT;
      return(fsize);
    }  
    value = m_v792->status_register_1;
  }

  // To be reviewed - TODO
  if (timeout < m_statistics->min_wait) m_statistics->min_wait = timeout;
  if (timeout > m_statistics->max_wait) m_statistics->max_wait = timeout;
  

  //For now we return the raw data from the V792
  while (1)
  {
    value = m_v792->status_register_2;
    if (value & 0x2)
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV792::getNextFragment: ERROR: Data buffer empty. No complete event found on channel " << m_channel_number);
      CREATE_ROS_EXCEPTION(wex2, V792Exception, USERWARNING, "DataChannelV792::getNextFragment: ERROR: Data buffer empty. No complete event found on channel " << m_channel_number);  
      ers::warning(wex2);

      //prepare an empty event
      *bufPtr = 0;
      fsize = 8;
      *status = S_NODATA;
      return(fsize);
    }
    
    data = m_v792->output_buffer;
    if (((data >> 24) & 0x7) == 2) //header found
    {
      //Write the total number of words
      num = (data >> 8) & 0x3f;
      *bufPtr++ = num + 2;
      fsize += 4;

      //Write the header
      *bufPtr++ = data;
      fsize += 4;
      break;
    }  
    else
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV792::getNextFragment: Read " << HEX(data) << " from V792 when waiting for header word on channel " << m_channel_number);
      CREATE_ROS_EXCEPTION(wex1, V792Exception, USERWARNING, "DataChannelV792::getNextFragment Read " << HEX(data) << " from V792 when waiting for header word");  
      ers::warning(wex1);
    }
  }
  
  toomuch = 0;
  //Write the data
  for (loop = 0; loop < num; loop++)
  {
    data = m_v792->output_buffer;
    if (fsize == (u_int)max_size)
      toomuch = 1;
      
    if (!toomuch)
    {
      *bufPtr++ = data;
      fsize += 4;
    }  
  }
  
  if (toomuch)
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV792::getNextFragment: max_size limit reached on channel " << m_channel_number << ". Data will be truncated now.");
    *status = S_OVERFLOW;
    return(fsize);
  }
  
  //Write the trailer
  if (fsize == (u_int)max_size)
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV702::getNextFragment: max_size limit reached on channel " << m_channel_number << ". Data will be truncated now.");
    *status = S_OVERFLOW;
    return(fsize);
  }
  
  data = m_v792->output_buffer;
  *bufPtr++ = data;
  fsize += 4;
 
  *status = S_OK;
  
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV792::My VMEbus pointer is = " << HEX((u_long)&m_v792->output_buffer));
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV792::leaving getNextFragment with fsize = " << fsize);
  return fsize;	// bytes ..
}


/************************************/
ISInfo *DataChannelV792::getISInfo()
/************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "V792DataChannel::getIsInfo: called");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "V792DataChannel::getIsInfo: done");
  return m_statistics;
}


/************************************/
void DataChannelV792::clearInfo(void)
/************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "V792DataChannel::clearInfo: entered");
  SingleFragmentDataChannel::clearInfo();
  m_statistics->min_wait = 0;
  m_statistics->max_wait = 0;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "V792DataChannel::clearInfo: done");
}




