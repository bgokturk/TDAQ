CMAKE_MINIMUM_REQUIRED(VERSION 2.6 FATAL_ERROR)

IF(NOT DEFINED ENV{RCDTDAQSETTINGS})
    MESSAGE(FATAL_ERROR "TDAQ environment not set up.\nSource setup_RCDTDAQ.sh")
ENDIF()

# Set project defaults. Have to appear before PROJECT command
SET(CMAKE_INSTALL_PREFIX
     "${CMAKE_CURRENT_LIST_DIR}/installed/$ENV{CMTCONFIG}"
     CACHE PATH "Installation path prefix")
SET(CMAKE_CXX_COMPILER
     "$ENV{TDAQ_INST_DIR}/LCG_$ENV{LCG_VERSION}/gcc/$ENV{GCC_VERSION}/$ENV{GCC_FLAVOUR}/bin/c++"
     CACHE PATH "Path to g++ executable")
SET(CMAKE_C_COMPILER
     "$ENV{TDAQ_INST_DIR}/LCG_$ENV{LCG_VERSION}/gcc/$ENV{GCC_VERSION}/$ENV{GCC_FLAVOUR}/bin/gcc"
     CACHE PATH "Path to gcc executable")

#SET(CMAKE_VERBOSE_MAKEFILE on)

SET(TDAQ_COVERAGE_LINK_FLAGS    "-Wl,--copy-dt-needed-entries")
SET(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${TDAQ_COVERAGE_LINK_FLAGS}")

# CMake generation starts here
PROJECT(RCDTDAQ)


LIST(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
INCLUDE(BL4Sutils)

SET(RCDTDAQ_BUILD CACHE BOOL ON)
SET(src_dir ${CMAKE_CURRENT_SOURCE_DIR})
SET(rosinfo_dir "${CMAKE_INSTALL_PREFIX}/../include/ROSInfo")

SET(BOOST_INCLUDE_DIR $ENV{BOOST_INCLUDE_DIR})
SET(BOOST_LIB_DIR $ENV{BOOST_LIB_DIR})

FIND_PACKAGE(ROOT REQUIRED COMPONENTS Core Eve)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ROOT_CXX_FLAGS}")

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -ggdb3 -DWORD64BIT")

INCLUDE_DIRECTORIES(
    $ENV{TDAQ_INST_PATH}/include
    $ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/include
    $ENV{TDAQC_INST_PATH}/include
    $ENV{DAQ_ROOT}/RCDTDAQ
    $ENV{DAQ_ROOT}/RCDTDAQ/rcd_v1290
    $ENV{DAQ_ROOT}/RCDTDAQ/rcd_v792
    $ENV{DAQ_ROOT}/RCDTDAQ/rcd_v560
    $ENV{DAQ_ROOT}/RCDTDAQ/rcd_eudaq
    $ENV{DAQ_ROOT}/RCDTDAQ/CommonLibrary
    ${CMAKE_INSTALL_PREFIX}/../include
    ${BOOST_INCLUDE_DIR}
    ${ROOT_INCLUDE_DIRS}
    ${BOOST_INCLUDE_DIR}
)
LINK_DIRECTORIES(
    $ENV{TDAQ_INST_PATH}/lib
    $ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/lib
    $ENV{TDAQC_INST_PATH}/lib
    ${BOOST_LIB_DIR}
    ${ROOTSYS}/lib
)

FILE(MAKE_DIRECTORY ${rosinfo_dir})
FILE(GLOB sources ${src_dir}/*.cc)
FILE(GLOB headers ${src_dir}/*.h)
SET(libs
   emon
   emon-dal 
   ohroot 
   Hist 
   cmdline 
)

ADD_SUBDIRECTORY(CommonLibrary)
ADD_SUBDIRECTORY(EventDumper)
ADD_SUBDIRECTORY(scaler)
ADD_SUBDIRECTORY(tdc)
ADD_SUBDIRECTORY(qdc)
ADD_SUBDIRECTORY(mimosa)
ADD_SUBDIRECTORY(RCDMonitor)
ADD_SUBDIRECTORY(microMegas)
ADD_SUBDIRECTORY(rcd_v1290)
ADD_SUBDIRECTORY(rcd_v560)
ADD_SUBDIRECTORY(rcd_v785)
ADD_SUBDIRECTORY(rcd_v792)
ADD_SUBDIRECTORY(rcd_trb)
ADD_SUBDIRECTORY(rcd_eudaq)
BL4S_ADD_EXTERNAL_PACKAGE(mmlib)

SET(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${TDAQ_COVERAGE_LINK_FLAGS}")
