#ifndef __EVENT_LOOPER_H__
#define __EVENT_LOOPER_H__

#include <time.h>

class OHRootProvider;
class BL4SConfig;

class EventLooper {
  public:
    EventLooper();
    template <typename EventReader, typename Source>
    void ProcessEvents(Source src, int nEvents, bool multiFileRun);

    void SetConfigFile(BL4SConfig * cfg) { m_cfg = cfg; }

    void SetPublishCycleTime(long int cycleTime) { m_publishCycleTime = cycleTime; }
    void SetHistProvider(OHRootProvider& prov) 
    {
      m_histProvider = &prov;
      m_publishHists = true;
    } 

    bool IsTimeToPublish();

    template <typename Monitor>
    void PublishHists(Monitor& monitor);

  private:
    OHRootProvider* m_histProvider;
    bool m_publishHists;
    std::string m_dataFile;
    long int m_publishCycleTime;
    time_t m_lastPublicationTime;
    BL4SConfig * m_cfg;

};


#endif
