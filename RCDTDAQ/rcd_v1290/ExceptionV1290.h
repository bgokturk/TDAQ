#ifndef V1290EXCEPTION_H
#define V1290EXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>
#include <iostream>

namespace RCD 
{
  using namespace ROS;

  class V1290Exception : public ROSException 
  {
  public:
    enum ErrorCode 
    {
      USERWARNING = 1,
      V1290_DUMMY2
    };
    V1290Exception(ErrorCode errorCode);
    V1290Exception(ErrorCode errorCode, std::string description);
    V1290Exception(ErrorCode errorCode, const ers::Context& context);
    V1290Exception(ErrorCode errorCode, std::string description, const ers::Context& context);
    V1290Exception(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

    virtual ~V1290Exception() throw() { }

  protected:
    virtual std::string getErrorString(u_int errorId) const;
  };
  
  inline std::string V1290Exception::getErrorString(u_int errorId) const 
  {
    std::string rc;
    switch (errorId) 
    {
    case USERWARNING:
      rc = "Something went wrong. Read on:";
      break;
    case V1290_DUMMY2:
      rc = "Dummy2";
      break;
    default:
      rc = "";
      break;
    }
    return rc;
  }
  
}
#endif //V1290EXCEPTION_H
