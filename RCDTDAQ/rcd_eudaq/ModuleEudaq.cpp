/********************************************************/
/*							*/
/* Date: 05.06.2019					*/ 
/* Author: C. Beirao / P. Schuetze			*/
/*							*/
/*** C 2015 - The software with that certain something **/

#include <unistd.h>
#include <iostream>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModules/ModulesException.h"
#include "rcd_eudaq/eudaq.h"
#include "rcd_eudaq/DataChannelEudaq.h"
#include "rcd_eudaq/ModuleEudaq.h"
#include "rcd_eudaq/ExceptionEudaq.h"


using namespace ROS;
using namespace RCD;


/**********************/
ModuleEUDAQ::ModuleEUDAQ()
/**********************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::constructor: Entered");
}


/**********************************/
ModuleEUDAQ::~ModuleEUDAQ()   noexcept
/**********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::destructor: Entered");

  while (m_dataChannels.size() > 0)
  {
    DataChannel *channel = m_dataChannels.back();
    m_dataChannels.pop_back();
    delete channel;		// we created them
  }
}


/************************************************************/
void ModuleEUDAQ::setup(DFCountedPointer<Config> configuration)
/************************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::setup: Entered");

  m_configuration = configuration;

  //get IP parameters for the EUDAQ MODULE
  m_port = configuration->getInt("IPport");
  m_id = configuration->getInt("channelId", 0);

 
  // timeout on UDP receive
  m_timeout = configuration->getInt("Timeout");

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::setup: IPport = " << m_port);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::setup: Timeout = " << m_timeout);
}

/*******************************************************/
void ModuleEUDAQ::configure(const daq::rc::TransitionCmd&)
/*******************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::configure: Entered");
  struct sockaddr_in server;

  //open UDP port...
  errno = 0;
  m_socket = socket(AF_INET, SOCK_DGRAM, 0);
  if (m_socket < 0)
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleEUDAQ::configure: Failed to create socket");
    char* strerr = strerror(errno);
    CREATE_ROS_EXCEPTION(ex1, EUDAQException, UDP_OPEN, strerr);
    throw ex1;

  }

// set timeout on socket
  struct timeval tv;
  tv.tv_sec = m_timeout;
  tv.tv_usec = 0;
  if (setsockopt(m_socket, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleEUDAQ::configure: Failed to set timeout");
    char* strerr = strerror(errno);
    CREATE_ROS_EXCEPTION(ex1, EUDAQException, UDP_OPEN, strerr);  // should really be TIMEOUT
    throw ex1;
  }

  int length_receive = sizeof(server);
  bzero(&server,length_receive);
  server.sin_family=AF_INET;
  server.sin_addr.s_addr=INADDR_ANY;
  server.sin_port=htons(m_port);
  errno = 0;
  int ret = bind(m_socket, (struct sockaddr *)&server,length_receive);
  if (ret <0)
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleEUDAQ::configure: Failed to bind socket");
    char* strerr = strerror(errno);
    CREATE_ROS_EXCEPTION(ex1, EUDAQException, UDP_BIND, strerr);
    throw ex1;
  }

  DataChannelEUDAQ *channel = new DataChannelEUDAQ(m_id, 0, m_socket, m_configuration);  //adapt to IP parameters
  m_dataChannels.push_back(channel);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::configure: Done ret=" << ret);

  //Initialize 
  // This is done by hand ...
}

/*********************************************************/
void ModuleEUDAQ::unconfigure(const daq::rc::TransitionCmd&)
/*********************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::unconfigure: Entered");

  //close the IP port
  int  ret;
  ret = close(m_socket);

  // TODO: Check about flushing the m_dataChannels

  if (ret <0)
  {
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleEUDAQ::configure: Failed to close socket");
    char* strerr = strerror(errno);
    CREATE_ROS_EXCEPTION(ex1, EUDAQException, UDP_CLOSE, strerr);
    throw ex1;
  }
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::unconfigure: Done");
}


/*****************************************************/
void ModuleEUDAQ::connect(const daq::rc::TransitionCmd&)
/*****************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::connect: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::connect: Done");
}


/********************************************************/
void ModuleEUDAQ::disconnect(const daq::rc::TransitionCmd&)
/********************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::disconnect: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::disconnect: Done");
}
    

int setSRSregistervalue(std::string, uint32_t, uint32_t, uint32_t, uint32_t, bool);

/***********************************************************/    
void ModuleEUDAQ::prepareForRun(const daq::rc::TransitionCmd&)
/***********************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::prepareForRun: Entered");

  //Tell the FEB to forget old data thay it may have buffered

// flush input
  unsigned int  buf[512];
  int n_while = 0;
  int n_bytes;
  do {
    errno = 0;
    n_bytes = recv(m_socket,buf,512*sizeof(unsigned int),MSG_DONTWAIT);
    if (n_bytes <0 && errno != EAGAIN)
    {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "ModuleEUDAQ::configure: Error on flush ");
      char* strerr = strerror(errno);
      CREATE_ROS_EXCEPTION(ex1, EUDAQException, UDP_OPEN, strerr);
      throw ex1;
    }
    n_while++;
  } while (errno != EAGAIN);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::prepareForRun: # packets flushed = " << (n_while-1));

/*****************  FEC ******************
  std::string ip = "10.0.0.2";   // hardwired, should go into OKS database
  setSRSregistervalue(ip, 6039, 0, 0xF, 1, false);   // SRS slow control manual
  std::ostringstream msgStream;
  msgStream << " FEC RO enabled";
  std::cout << msgStream.str() << std::endl;  // logfile
  ERS_REPORT_IMPL(ers::warning, ers::Message, msgStream.str(), );
*******************************************/
  
  for (auto ch : m_dataChannels){
    static_cast<DataChannelEUDAQ*>(ch)->resetEventCounter();
  }

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::prepareForRun: Done");
}


/****************************************************/
void ModuleEUDAQ::stopDC(const daq::rc::TransitionCmd&)
/****************************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::stopDC: Entered");

/*****************  FEC ******************
  std::string ip = "10.0.0.2";   // hardwired, should go into OKS database
  setSRSregistervalue(ip, 6039, 0, 0xF, 0, false);   // SRS slow control manual
  std::ostringstream msgStream;
  msgStream << " FEC RO disabled";
  std::cout << msgStream.str() << std::endl;  // logfile
  ERS_REPORT_IMPL(ers::warning, ers::Message, msgStream.str(), );
********************************************/

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::stopDC: Done");
}


/*********************************************/
DFCountedPointer <Config> ModuleEUDAQ::getInfo()
/*********************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::getInfo: Entered");
  DFCountedPointer<Config> info = Config::New();

  //info->set("EUDAQ Firmware revision", m_EUDAQ->firmware_revision);   //replace by something useful
   
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::getInfo: Done");
  return(info);
}


/**************************/
void ModuleEUDAQ::clearInfo()
/**************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::clearInfo: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "ModuleEUDAQ::clearInfo: Done");
}

//FOR THE PLUGIN FACTORY
extern "C" 
{
  extern ReadoutModule* createReadoutModuleEUDAQ();
}
ReadoutModule* createReadoutModuleEUDAQ()
{
  return (new ModuleEUDAQ());
}
