/********************************************************/
/*							*/
/* Date: 05.06.2019					*/ 
/* Author: C. Beirao / P. Schuetze			*/
/*							*/
/* add error codes for UDP                              */
/*                                                      */
/*                                                      */
/*** C 2015 - The software with that certain something **/

#ifndef EUDAQEXCEPTION_H
#define EUDAQEXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>
#include <iostream>

namespace RCD 
{
  using namespace ROS;

  class EUDAQException : public ROSException 
  {
  public:
    enum ErrorCode 
    {
      UDP_OPEN = 100,
      UDP_BIND,
      UDP_CLOSE,
      UDP_RECEIVE
    };
    EUDAQException(ErrorCode error);
    EUDAQException(ErrorCode error, std::string description);
    EUDAQException(ErrorCode error, const ers::Context& context);
    EUDAQException(ErrorCode error, std::string description, const ers::Context& context);
    EUDAQException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

    virtual ~EUDAQException() throw() { }

  protected:
    virtual std::string getErrorString(u_int errorId) const;
  };

  inline EUDAQException::EUDAQException(EUDAQException::ErrorCode error, std::string description)
   : ROSException("EUDAQModule",error,getErrorString(error),description) { }

  inline EUDAQException::EUDAQException(EUDAQException::ErrorCode error)
   : ROSException("EUDAQModule",error,getErrorString(error)) { }

  inline EUDAQException::EUDAQException(EUDAQException::ErrorCode error, std::string description, const ers::Context& context)
   : ROSException("EUDAQModule",error,getErrorString(error),description,context) { }

  inline EUDAQException::EUDAQException(EUDAQException::ErrorCode error, const ers::Context& context)
   : ROSException("EUDAQModule",error,getErrorString(error),context) { }

  inline EUDAQException::EUDAQException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context)
                                 : ROSException(cause, "EUDAQModule", error, getErrorString(error), description, context) {}

  
  inline std::string EUDAQException::getErrorString(u_int errorId) const 
  {
    std::string rc;
    switch (errorId) 
    {
    case UDP_OPEN:
      rc = " UDP Open socket failed";
      break;
    case UDP_BIND:
      rc = " UDP Bind socket failed";
      break;
    case UDP_CLOSE:
      rc = " UDP Close socket failed";
      break;
    case UDP_RECEIVE:
      rc = " UDP receive failed";
      break;
    default:
      rc = "";
      break;
    }
    return rc;
  }
  
}
#endif //EUDAQEXCEPTION_H
