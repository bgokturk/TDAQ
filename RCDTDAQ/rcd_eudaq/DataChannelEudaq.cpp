/********************************************************/
/*							*/
/* Date: 05.06.2019					*/ 
/* Author: C. Beirao / P. Schuetze			*/
/* Copy of the TRB code for reception of eudaq data	*/
/*							*/
/*** C 2015 - The software with that certain something **/

#include <unistd.h>


#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFDebug/DFDebug.h"
#include "DFSubSystemItem/Config.h"
#include "rcd_eudaq/ExceptionEudaq.h"
#include "rcd_eudaq/eudaq.h"
#include "rcd_eudaq/DataChannelEudaq.h"

using namespace ROS;
using namespace RCD;

/**************************************************************************/
DataChannelEUDAQ::DataChannelEUDAQ(u_int channelId,
				 u_int channelIndex,
				 int eudaqsocket,
				 DFCountedPointer<Config> configuration,
				 DataChannelEUDAQInfo *info) :
  SingleFragmentDataChannel(channelId, channelIndex, 0, configuration, info)
/**************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelEUDAQ::constructor: Entered");
  m_statistics = info;
  m_channelId = channelId;
  m_socket = eudaqsocket;
  m_busyDelay = configuration->getInt("BusyDelay");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelEUDAQ::constructor: channelId = " << m_channelId);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelEUDAQ::constructor: sock = " << m_socket);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelEUDAQ::constructor: busyDelay = " << m_busyDelay);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelEUDAQ::constructor: Done");
}


/*********************************/
DataChannelEUDAQ::~DataChannelEUDAQ() 
/*********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelEUDAQ::destructor: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelEUDAQ::destructor: Done");
}


/****************************************************************************/
int DataChannelEUDAQ::getNextFragment(u_int* buffer, int max_size,
                                    u_int* status, u_long /*pciAddress*/)
/****************************************************************************/
{
  int fsize = 0;
  socklen_t fromlen;
  struct sockaddr_in from;
  fromlen = sizeof(struct sockaddr_in);
  u_int l_status = S_OK;

  ++m_nEvents;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15,
             "DataChannelEUDAQ::getNextFragment: Entered for channel = " << std::hex <<
             m_channel_number << std::dec);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelEUDAQ::getNextFragment: max_size = " << max_size);

  u_int ip4;		// IP addrees without dots

  u_int* curBufPtr = buffer;
  u_int recordedFrames = 0;

  *curBufPtr++ = m_channelId;
  *curBufPtr++ = EUDAQ_MIMOSA;
  ++curBufPtr;

  u_int firstWord;

  fsize = 3*4;
  int curMaxSize = max_size - fsize;	//bytes

  for(int frame = 0; frame < 2; ++frame)
  {
    int n_bytes = recvfrom(m_socket,
                           curBufPtr+1, // We want to add IP address, # words and remove UDP header
                           curMaxSize - 4,
                           0,
                           (struct sockaddr *)&from,
                           &fromlen);

    if ((n_bytes == -1) && (errno == EAGAIN)) { // timeout
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelEUDAQ::getNextFragment: Timeout ");
      std::ostringstream msgStream;
      msgStream << "DataChannelEUDAQ: Timeout (Event: " << m_nEvents << ", received data: "
                << recordedFrames << " Frames)";
      std::cout << msgStream.str() << std::endl;
      ERS_REPORT_IMPL(ers::warning, ers::Message, msgStream.str(), );

      //prepare an empty event
      *curBufPtr = EUDAQ_MIMOSA_TRAILER;
      buffer[2] = fsize + 4;

      *status = S_TIMEOUT;
      return(fsize + 4);
    }

    if (n_bytes< 0) {
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelEUDAQ::getNextFragment: Failed to receive");
      char* strerr = strerror(errno);
      CREATE_ROS_EXCEPTION(ex1, EUDAQException, UDP_RECEIVE, strerr);
      throw ex1;
    }

    // Check if the UDP header is correct
    firstWord = curBufPtr[1];
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelEUDAQ::getNextFragment:  first word =  "
               <<  std::hex << firstWord << std::dec);
    if(firstWord != 0x0D15EA5E) {
      std::ostringstream msgStream;
      msgStream << "DataChannelEUDAQ: Wrong UDP header (Event: " << m_nEvents << ", received header: "
                << firstWord;
      std::cout << msgStream.str() << std::endl;
      ERS_REPORT_IMPL(ers::warning, ers::Message, msgStream.str(), );

      //prepare an empty event
      *curBufPtr = EUDAQ_MIMOSA_TRAILER;
      buffer[2] = fsize + 4;

      *status = S_TIMEOUT;
      return(fsize + 4);
    }

    // Get final pieces of information to put at start of frame
    ip4 = ntohl(from.sin_addr.s_addr);
    curBufPtr[0] = ip4;
    curBufPtr[1] = n_bytes/sizeof(int32_t);

    fsize += n_bytes;
    curMaxSize -= n_bytes;
    curBufPtr += (n_bytes/sizeof(int32_t));

    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelEUDAQ::getNextFragment:  curBufPtr "  << curBufPtr);
    DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelEUDAQ::getNextFragment:  curMaxSize "  << curMaxSize);
  }

  *curBufPtr = EUDAQ_MIMOSA_TRAILER;
  fsize += 4;
  buffer[2] = fsize;

  *status = l_status;

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelEUDAQ::getNextFragment getNextFragment with fsize = " << fsize);
  return fsize;
}


/**********************************/
ISInfo *DataChannelEUDAQ::getISInfo()
/**********************************/
{
//  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "EUDAQDataChannel::getIsInfo: called");
//  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "EUDAQDataChannel::getIsInfo: done");
  return m_statistics;
}


/************************************/
void DataChannelEUDAQ::clearInfo(void)
/************************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "EUDAQDataChannel::clearInfo: entered");
  SingleFragmentDataChannel::clearInfo();
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "EUDAQDataChannel::clearInfo: done");
}




