INFO_COLOR='\e[92m'
WARN_COLOR='\e[93m'
ERROR_COLOR='\e[91m'
RESET_COLOR='\e[0m'

ROOT=$(pwd -P)
RE_START="\bstart\b"
RE_CLEAN="\bclean\b"
RE_OHP="\bohp\b"

msg() {
  case "$1" in 
    "error") echo -e "${ERROR_COLOR}[ERROR]: $2${RESET_COLOR}";;
    "warn") echo -e "${WARN_COLOR}[WARNING]: $2${RESET_COLOR}";;
    "info") echo -e "${INFO_COLOR}[INFO]:$2${RESET_COLOR}";;
  esac
}


