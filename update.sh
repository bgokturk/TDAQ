#!/bin/bash

# Make sure that the local repository has both the branch and the remote correctly configured
# You might need to set them up, for instance:
#   git remote add compile atlas40.desy.de:/home/bl4sdaq/TDAQ
#   git fetch --all
#   git checkout --track compile/eudaq
BRANCH=eudaq
GITREMOTE=compile
REMOTE=atlas40.desy.de

# First make sure the remote is not currently on the branch we want to work on
ssh $REMOTE "cd ~/TDAQ && git checkout master"

# Do the synchronization
git pull $GITREMOTE $BRANCH
rsync -avzhe ssh --progress bl4sdaq@$REMOTE:~/TDAQ/RCDTDAQ/ RCDTDAQ/
git push $GITREMOTE $BRANCH

# Bring the remote back to the working branch
ssh $REMOTE "cd ~/TDAQ && git checkout $BRANCH"
