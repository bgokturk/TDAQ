#!/bin/bash

# Store arguments in variable and reset. This is necessary
# as otherwise they will be passed to sourced scripts
declare -a args
args=($*)
shift $#

# Get directory path of this file
ROOT=$(readlink -f ${BASH_SOURCE[0]})
ROOT=$(dirname ${ROOT})
export DAQ_ROOT=${DAQ_ROOT:-${ROOT}}
pushd . >/dev/null
cd ${ROOT}

source toolFunctions.sh || return 1
export LC_ALL=C

if [ -n "${TDAQ_PARTITION}" ]; then
  if [ -z "${RCDTDAQSETTINGS}" ]; then
    msg 'error' 'TDAQ set up by another script. Please start with a fresh shell!'
    return 2
  fi
  msg 'warn' "TDAQ already set up !!!"
else
  # source current settings and export relevant variables
  if [ ! -f RCDTDAQ_settings.sh ]; then
    if [ -f RCDTDAQ_settings.sh.example ]; then
      cp RCDTDAQ_settings.sh.example RCDTDAQ_settings.sh
    else
      echo "RCDTDAQ_settings.sh: file not found"
      exit 1
    fi
  fi
  source RCDTDAQ_settings.sh || return 1
  oksdir=${DAQ_ROOT}/OKS/current
  if [ ! -d ${oksdir}/hw -o ! -d ${oksdir}/partitions -o \
      ! -d ${oksdir}/segments ]; then
    echo "Could not find OKS database files in ${oksdir}." >&2
    echo "You can copy a configuration from ${ROOT_DIR}/OKS to that directory." >&2
    exit 1
  fi
  export TDAQ_OUTPUT_RAWDATA
  export TDAQ_RELEASE
  export BL4S_CONFIGS
  export BOOST_VERSION
  export GCC_VERSION
  export GCC_FLAVOUR=${TDAQ_FLAVOUR%-*}
  export LCG_VERSION
  export TDAQ_INST_DIR
  export TDAQ_FLAVOUR

  #export BL4S_ANALYSIS_PATH=${DAQ_ROOT}/Analysis
  #export BL4SDATA=/afs/cern.ch/user/d/daquser/public/rawdata
  export BL4S_ANALYSIS_PATH=/afs/cern.ch/user/d/daquser/public/Analysis
  #if [ ! -d ${BL4S_ANALYSIS_PATH} ]; then
  #msg 'warn' "Environment variable BL4S_ANALYSIS_PATH is incorect!\n $BL4S_ANALYSIS_PATH \n  does NOT exists!\n"
  #fi
  #export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${BL4S_ANALYSIS_PATH}/lib
  #export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${BL4S_MONITORING_PATH}/lib
  export CLASSPATH
  export TDAQ_CLASSPATH
  export LD_LIBRARY_PATH
  export TDAQ_DIR
  #export CMTPROJECTPATH=/afs/cern.ch/atlas/offline/external:/afs/cern.ch/atlas/project/tdaq/cmt
  if [[ "${args[*]}" =~ "debug" ]]; then
    echo "Building code for debugging"
    export TDAQ_ERS_DEBUG_LEVEL=${args[1]:?Usage:. setup_RCDTDAQ,sh debug LEVEL}
    export CMTCONFIG=${TDAQ_FLAVOUR}-dbg
    export BUILD_TYPE=DBG
  else
    echo "Building optimized code"
    export TDAQ_ERS_DEBUG_LEVEL=0
    export CMTCONFIG=${TDAQ_FLAVOUR}-opt
    export BUILD_TYPE=OPT
  fi


  # Set AFS paths if the service is running and requested by settings
  TDAQ_INST_DIR=$LOCAL_ATLAS_TDAQ_INST_DIR
  isAFSrunning=0
  service afs status > /dev/null 2>&1 && isAFSrunning=1
  if [ ${USE_AFS} -eq 1 ]; then
    if [ ${isAFSrunning} -eq 1 ]; then
      #Check availability of afs and if TDAQ installation can be reached
      tdaqAfsDir=${AFS_ATLAS_TDAQ_INST_DIR}/tdaq/${TDAQ_RELEASE}
      if [ -d $tdaqAfsDir ]; then
        TDAQ_INST_DIR=${AFS_ATLAS_TDAQ_INST_DIR}
      else
        msg warn "Could not find ${TDAQ_RELEASE} in AFS Falling back to local installation"
      fi
    else
      msg warn "AFS not running. Falling back to local installation"
    fi
    unset tdaqAfsDir
  fi
  
  tdaqSetupFile=${TDAQ_INST_DIR}/tdaq/${TDAQ_RELEASE}/installed/setup.sh 
  if [ -f ${tdaqSetupFile} ]; then
    echo "Setting LOCAL Atlas TDAQ ${TDAQ_RELEASE#tdaq-} environment"

    source ${TDAQ_INST_DIR}/LCG_${LCG_VERSION}/gcc/${GCC_VERSION}/${GCC_FLAVOUR}/setup.sh || return 1
    boostReleaseVersion=$(echo ${BOOST_VERSION:0:4} | tr "." "_")
    export BOOST_ROOT=${TDAQ_INST_DIR}/LCG_${LCG_VERSION}/Boost/${BOOST_VERSION}/${CMTCONFIG}/
    export BOOST_INCLUDE_DIR=${BOOST_ROOT}/include/boost-${boostReleaseVersion}
    export BOOST_LIB_DIR=${BOOST_ROOT}/lib
    export BOOST_INCLUDEDIR
  
    source ${tdaqSetupFile} || return 1
    source ${TDAQ_INST_DIR}/LCG_${LCG_VERSION}/ROOT/${ROOT_VERSION}/${CMTCONFIG}/bin/thisroot.sh || return 1
    export PATH=$PATH:${DAQ_ROOT}/RCDTDAQ/installed/${CMTCONFIG}/bin
    export TDAQ_RELEASE_DIR="${TDAQ_INST_DIR}/tdaq/${TDAQ_RELEASE}/installed"
    export CPATH=${TDAQ_RELEASE_DIR}/include
    CPATH=${CPATH}:${TDAQ_RELEASE_DIR}/${CMTCONFIG}/include
    CPATH=${CPATH}:${TDAQ_INST_DIR}/tdaq-common/tdaq-common-${TDAQ_COMMON_RELEASE}/installed/include
    CPATH=${CPATH}:${BOOST_INCLUDE_DIR}
    unset boostReleaseVersion
  else
    echo "${tdaqSetupFile}: access denied!"
    return 1
  fi
  unset tdaqSetupFile

  # Check CMake
  if [ -z "$(which cmake 2>/dev/null)" ];then
    if [ -x "${CMAKE_BIN_DIR}/cmake" ];then
      export PATH="${CMAKE_BIN_DIR}:${PATH}"
    else
      msg "error" "CMake not found. Check paths."
      return 1
    fi
  fi
  # Setup ROOT
  source ${TDAQ_INST_DIR}/LCG_${LCG_VERSION}/ROOT/${ROOT_VERSION}/${CMTCONFIG}/bin/thisroot.sh || return 1

  # Use a local IPC server
  IPC_INIT_FILE=${DAQ_ROOT}/ipc_root.ref
  export TDAQ_IPC_INIT_REF=file:${IPC_INIT_FILE}
  if [ ! -e ${IPC_INIT_FILE} ]; then
    touch ${IPC_INIT_FILE}
  fi

  # Configure the RCDTDAQ partition
  export TDAQ_RCDTDAQ_ROOT=${DAQ_ROOT}/RCDTDAQ
  export TDAQ_PARTITION=partRCDTDAQ
  export TDAQ_APPLICATION_NAME=RCDApp
  export TDAQ_DB_NAME=partRCDTDAQ
  export TDAQ_DB_DATA=${TDAQ_RCDTDAQ_ROOT}/installed/share/data/daq/partitions/${TDAQ_DB_NAME}.data.xml
  export TDAQ_DB_PATH=${TDAQ_RCDTDAQ_ROOT}/installed/share/data:${TDAQ_DB_PATH}
#  export TDAQ_DB=oksconfig:${TDAQ_DB_DATA}

  # Add our libraries to the searching paths 
  export LD_LIBRARY_PATH=${TDAQ_RCDTDAQ_ROOT}/installed/${CMTCONFIG}/lib:${LD_LIBRARY_PATH}
  #export LD_LIBRARY_PATH=${TDAQ_RCDTDAQ_ROOT}/installed/${CMTCONFIG}/lib:${BL4S_ANALYSIS_PATH}/lib:${BL4S_MONITORING_PATH}/lib:${LD_LIBRARY_PATH}
  export LIBRARY_PATH=${LD_LIBRARY_PATH}:${LIBRARY_PATH}
  export PATH=${TDAQ_RCDTDAQ_ROOT}/installed/${CMTCONFIG}/bin:${PATH}
fi # [ -n "${TDAQ_PARTITION}" ]


if [[ "${args[*]}" =~ $RE_CLEAN ]]; then
  msg 'info' "Cleaning up the partition..."
  pmg_kill_partition -p ${TDAQ_PARTITION}
  pmg_kill_partition -p initial
  sleep 4
  #pmg_killall_everywhere
  pmg_kill_agent
  sleep 2
  pkill -U $UID  pmgserver
  killall pmglauncher

  #for host in $(rc_checkapps | awk '{print $4}' | grep -e '.\+' | uniq)
  #do
  #msg 'info' "Killing PMG on host: ${host}"
  #ssh $host 'pkill pmgserver; killall pmglauncher'
  #done
  killall ipc_server
  killall ohp
  msg 'info' 'ps aux | grep -v sshd | grep -i $USER'
  sleep 3
  ps aux | grep -v sshd | grep -i $USER
fi


if [[ "${args[*]}" =~ $RE_START ]]; then
  # Initialise the partition, along with the IGUI
  setup_daq -ng -p ${TDAQ_PARTITION}
  unset START
fi

popd >/dev/null
unset args

