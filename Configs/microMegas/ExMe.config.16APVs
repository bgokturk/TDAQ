# mmdaq configuration for daq tests
# last modified: M.Byszewski 07/27/2012  
# last modified: J.Petersen 2017/01/07  
# test with 16 APVS on ExMe1,ExMe2  17/04/13
# 

############################################################
# network parameters of the DAQ
############################################################
mmdaq.DAQIPAddress: 10.0.0.3
mmdaq.DAQIPPort: 6006

############################################################
#data saving
############################################################
mmdaq.LogFile: 			/afs/cern.ch/work/d/daquser/public/mMdata/log/mmdaq_ExMe_2017.log
mmdaq.ChannelMapFile: 		ExMe.map
mmdaq.CrosstalkCorrectionFile: 	crosstalk.map
# save data to this directory:
mmdaq.WriteDataPath: 		/afs/cern.ch/work/d/daquser/public/mMdata
# factor for zero suppression.  Set to 0.01 to keep all events 17/01 JOP
mmdaq.ZeroTresholdFactor: 1.0
# keep PedestalEventsPerSave = 0 for compatibility, 
# will be used for automatic pedestal measurements
mmdaq.PedestalEventsPerSave: 0
mmdaq.PathToPedestalFile: 	/afs/cern.ch/work/d/daquser/public/mMdata/pedestals


############################################################
# FEC IP address, connection to APV Chips
############################################################
mmdaq.FECs: FEC1
#mmdaq.FEC1.Chips: APV0, APV1, APV2, APV3
mmdaq.FEC1.Chips: APV0, APV1, APV2, APV3, APV4, APV5, APV6, APV7, APV8, APV9, APV10, APV11, APV12, APV13, APV14, APV15
#mmdaq.FEC1.IPAddress: 10.0.0.11
# Saclay FEC
mmdaq.FEC1.IPAddress: 10.0.0.2


############################################################
# translate names like APV0 to their local id numbers (0-15)
# usually you can leave this as it is
############################################################
mmdaq.FEC1.Chips.APV0.Id: 0
mmdaq.FEC1.Chips.APV1.Id: 1
mmdaq.FEC1.Chips.APV2.Id: 2
mmdaq.FEC1.Chips.APV3.Id: 3
mmdaq.FEC1.Chips.APV4.Id: 4
mmdaq.FEC1.Chips.APV5.Id: 5
mmdaq.FEC1.Chips.APV6.Id: 6
mmdaq.FEC1.Chips.APV7.Id: 7
mmdaq.FEC1.Chips.APV8.Id: 8
mmdaq.FEC1.Chips.APV9.Id: 9
mmdaq.FEC1.Chips.APV10.Id: 10
mmdaq.FEC1.Chips.APV11.Id: 11
mmdaq.FEC1.Chips.APV12.Id: 12
mmdaq.FEC1.Chips.APV13.Id: 13
mmdaq.FEC1.Chips.APV14.Id: 14
mmdaq.FEC1.Chips.APV15.Id: 15

############################################################
# CHAMBERS, list of names, strips, their connection to APV chips
############################################################
#Chambers are ordered from front to back of beam incidence
mmdaq.Chambers: ExMe1, ExMe2

mmdaq.Chamber.ExMe1.ZPos: 1
mmdaq.Chamber.ExMe2.ZPos: 2

mmdaq.Chamber.ExMe1.DriftGap: 5.0
mmdaq.Chamber.ExMe2.DriftGap: 5.0

mmdaq.Chamber.ExMe1.Strips: X
mmdaq.Chamber.ExMe1.Strips.X.Angle: 90.0
mmdaq.Chamber.ExMe1.Strips.X.Chips: APV0, APV1, APV2, APV3, APV4, APV5, APV6, APV7 
mmdaq.Chamber.ExMe1.Strips.X.Pitch: 0.45
mmdaq.Chamber.ExMe1.Strips.X.Min: 1
mmdaq.Chamber.ExMe1.Strips.X.Max: 1024

mmdaq.Chamber.ExMe2.Strips: X
mmdaq.Chamber.ExMe2.Strips.X.Angle: 90.0
mmdaq.Chamber.ExMe2.Strips.X.Chips: APV8, APV9, APV10, APV11, APV12, APV13, APV14, APV15
mmdaq.Chamber.ExMe2.Strips.X.Pitch: 0.45
mmdaq.Chamber.ExMe2.Strips.X.Min: 1
mmdaq.Chamber.ExMe2.Strips.X.Max: 1024

